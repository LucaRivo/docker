# Docker Fundamental

## Introduzione

Nella prima parte del workshop vengono introdotti i concetti di base, relativi alle immagini ed ai container.

Il workshop è basato principalmente su esercizi pratici, da effettuare sul proprio sistema preconfigurato in Cloud.

Per accedere si può utilizzare un client SSH, come ad esempio Putty. 

Per l'accesso occorre inserire nel campo Hostname il valore comunicato all'inizio del workshop ed inoltre configurare la chiave privata come nell'esempio sotto riportato.

![Putty](./labs/images/accesso_con_putty.png)

## Agenda

Docker Fundamentals

1. [Architettura Docker](./labs/exercise0.0.md)
2. [Operazioni di base sulle Immagini](./labs/exercise1.0.md)
3. [Layers](./labs/exercise1.1.md)
4. [Tag](./labs/exercise1.2.md)
5. [Utilizzo di Container](./labs/exercise2.0.md)
6. [Operazione di Inspect, Cenni di Network](./labs/exercise2.3.md)
6. [Mapping Porte, Variabili Ambiente](./labs/exercise2.4.md)
7. [Gestione delle Risorse](./labs/exercise2.8.md)

[Data Management](./labs/exercise4.0.md)

1. [Volumi](./labs/exercise4.1.md)
2. [Bind Mounts](./labs/exercise4.2.md)
3. [Volumi definiti a livello di Dockerfile](./labs/exercise4.3.md)

[Esercizi Riassuntivi](./labs/exercise4.4.md)

## Obiettivi

In questa sezione vengono presentati i concetti di base di Docker ed i relativi comandi di utilizzo.

Per iniziare, verifichiamo [la corretta installazione di Docker](./labs/exercise0.0.md).

