# Tag Images

## Introduzione

Alle immagini si possono quindi associare uno o più TAG, come si può vedere andando su Docker Hub:

https://hub.docker.com/explore/

Un TAG è quindi un modo per creare alias della stessa immagine. Questa funzionalità permette di implementare varie policy di tagging, sia statiche che dinamiche.



Il comando per taggare un’immagine ha la seguente sintassi:

```docker image tag <IMMAGINE_da_TAGGARE> <IMMAGINE:TAG_DA_ATTRIBUIRE>```

Come esercizio, aggiungere tag all'immagine tomcat:8 e controllare l'output con ```docker images```



## Delete Tag

Per cancellare un tag da una immagine si utilizza:


```docker rmi <TAG>```

## Hands On

Esercizio pratico sul tagging dinamico utilizzando le immagini batch_v1.tar, batch_v2.tar, batch_v3.tar

Come prerequisito, caricare tramite operazione di load le immagini dai files tar e controllare l'esito dell'operazione con:

```docker images --filter "label=application=Workshop-Batch"```



Domanda: Supponendo di avere due immagini taggate con my_application:2 e my_application:2.0 rappresentano la stessa immagine ?



# Riassunto concetti Immagini

- gerarchia di livelli ( layer )

- usa un filesystem di tipo Union per aggregare piu' layer e presentarli come un unico filesystem

- readonly, una volta creato un layer di un’image non si puo' modificare, ma solo sovrascrivere

- usata per eseguire un container

- usa meccanismi di cache per i layer

- ha un ID univoco

- ha uno o piu' tag per identificarne la versione, di default e' “latest”





[Sezione container](./exercise2.0.md)









