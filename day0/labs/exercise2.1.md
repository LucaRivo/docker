# Docker Container


Aggiungendo al commando docker ps l’opzione –a viene presentato tutto l’elenco dei container presenti, compresi quelli non running. 
I dati che vengono presentati sono:

- l'id del container
- il nome dell'immagine utilizzata
- il comando utilizzato per l'avvio
- il tempo trascorso dalla creazione
- lo stato (per esempio se è "up")
- le eventuali porte mappate
- il nome del container

## Lifecycle

Per quanto riguarda gli stati, lo schema che li rappresenta si può così schematizzare:

![Docker Container Lifecycle](./images/docker-container-lifecycle.png)

## Esecuzione di un comando all’interno di un container

E’ possibile eseguire uno specifico comando all’interno di un container. Ad esempio:

```docker exec -it tomcat1 /bin/bash```

##  Restart Policy

TO DO

## Mapping delle porte

Per poter utilizzare i servizi esposti da un container è necessario effettuare un forward del traffico tra le porte dell’host verso le porte che espone il container.

![Docker Port Mapping](./images/docker-port-mapping.png)

```docker run -d --name tomcat -p 8888:8080 tomcat:8```

Si possono esporre più anche più porte , aggiungendo altre opzioni –p

```docker run -d --name tomcat -p 8888:8080 –p 8005:8005 tomcat:8```

nel caso si debba utilizzare il protocollo UDP la sintassi è:

```-p 53160:53160 -p 53160:53160/udp```

## Environment Variables

Ad un container si possono passare delle variabili di ambiente. Questo meccanismo può essere sfruttato per effettuare operazioni di configurazione

L’opzione da utilizzare in questo caso è:

```docker run -e MIA_VARIABILE=”pippo” –e ALTRA_VARIABILE=123 …```

Come esempio, si utilizzi l’immagine mendhak/udp-listener che implementa un UDP listener. I messaggi ricevuti via UDP vengono scritti su log.La porta di ascolto può essere configurata tramite la variabile di ambiente UDPPORT.

Mandare in esecuzione un container che sia ascolto nella porta 7777 dei pacchetti UDP.

Come client, da utilizzare per un test funzionale, si può utilizzare il comando:

```nc -u localhost UDPPORT```

[Container Inspection](./exercise2.3.md).