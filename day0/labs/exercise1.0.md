# Docker Images

Le immagini presenti nel file system locale si ottengono con: 

```bash
docker images
```

L’operazione di download di un’immagine da un registry si effettua con il comando pull:

```bash
docker pull tomcat:8
```

In questo esempio viene scaricata l'immagine tomcat con il tag 8, dal registry di default ( Docker Hub ).

Durante il download vengono visualizzati i layer ( di seguito trattati ) che compongono l’immagine.



Per scaricare un’immagine da uno specifico registry, occorre specificare il FQDN del repository. Ad esempio:

```bash
docker pull gcr.io/google-containers/busybox:latest
```

## Processo creazione Immagini


Le immagini vengono create a partire da un Dockerfile dove vengono indicate le istruzioni con cui effettuare la build, a partire da un’altra immagine esistente.

![Processo creazione immagini](./images/docker-processo-build.png)


## Cancellazione di Immagini

Le immagini possono essere cancellate dal disco locale tramite:

```bash
docker rmi <NOME_IMMAGINE>
```

quindi, ad esempio

```bash
docker rmi tomcat:8
```

## Distribuzione immagine tramite file

Un’immagine può anche essere esportata all’interno di un file.tar, copiata su un diverso host e successivamente caricata con i comandi:

```bash
docker image save <IMAGE> > my_file.tar
docker image load -i my_file.tar
```

Ad esempio

```bash
docker image save tomcat:8 > tomcat8.tar
docker image load -i tomcat8.tar
```
In versioni più recenti il comando è stato modificato in:

```bash
docker save -o <path for generated tar file> <image name>
docker load -i <path to image tar file>
```

Quindi, ad esempio, per salvare un'immagine in un file:

```bash
docker save -o ./tomcat80.tar tomcat:8.0
```

Questo metodo può essere utilizzato come workaround, per distribuire immagini, senza dover utilizzare necessariamente un registry.
Può essere utile in determinate occasioni, ma le best practice indicano che questa modalita' deve essere l'eccezione piuttosto che la regola.

## Inspect di un'immagine 

Tutti i dettagli su una specifica immagine si ricavano utilizzando l’operazione di inspect.
Operazione disponibile non solo per le immagini, ma anche per container, volumi,...

```bash

docker image inspect <IMAGE>

```
Esempio: ```docker image inspect tomcat:8 ```

L'output è in formato JSON e mostra informazioni e metadati relativi all'immagine.

Il risultato può essere filtrato per una maggiore leggibilità. Ad esempio:

```bash
docker inspect -f '{{.ContainerConfig.Env}}' tomcat:8
```

```bash
docker inspect -f '{{.ContainerConfig.Labels}}' centos
```

## Ricerca di immagini

Esiste una funzionalità che permette la ricerca di immagini, dal repository Docker Hub:

```bash
docker search busybox
```

Domanda: L'immagine busybox risultante dal search è la stessa scaricata da google-containers ?



[Sezione Layers](./exercise1.1.md)