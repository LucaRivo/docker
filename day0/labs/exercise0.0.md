# Verifica Installazione

La verifica della corretta installazione di Docker e del suo funzionamento si effettua con il comando:

```bash
docker version
```

Se l’utente con cui si esegue il comando non appartiene al gruppo docker è necessario configurare ed utilizzare la modalità sudo.

Il comando mostra la versione delle componenti client e server, secondo lo schema architetturale di Docker, sotto riportato.

Da notare che le due componenti non necessariamente devono essere nella stessa versione, tuttavia:

- se il demone è di una versione superiore al client, il client non può sfruttare tutte le funzionalità disponibili

- viceversa, se il client è di versione superiore al demone, potrebbe richiedere delle funzionalità che a livello di demone non sono ancora presenti

Nel caso il demone non risponda, come prima verifica , si può controllare lo stato del servizio Docker:

```bash
sudo service docker status
```

# Architettura Docker



![Architettura Docker ](./images/docker_architecture.png)


Docker utilizza un'architettura client-server. Il client viene usato per impartire comandi per la gestione dei container, che vengono eseguiti nella parte server. 
Client/Server possono stare nello stesso sistema ( caso postazione sviluppo locale ) oppure su sistemi diversi ( caso piattaforma centrale utilizzata da un sistema client remoto).
La comunicazione Client/Server avviene tramite REST API, utilizzando UNIX sockets od una interfaccia di rete.

Un’immagine può essere vista come una sorta di read-only template, che viene utilizzata per «istanziare» N container.

Un container in esecuzione non è altro che un’istanza di un’immagine. Quando chiediamo a Docker di eseguire un container a partire da una determinata immagine, quest’ultima deve essere presente sul disco locale.

In caso contrario, Docker provvederà a scaricarla in maniera autonoma ( operazione di pull ) da appositi registry.

Il registry di default è Docker Hub. Esistono anche altri registry pubblici ed è possibile utilizzare registry privati.

Nei repository pubblici si trovano numerose immagini, pronte per l’uso ( prodotti di middleware, linguaggi di programmazione, …). Nei repository privati invece, ogni organizzazione può pubblicare e condividere i propri servizi/componenti contenerizzati.


# Verifica esecuzione di un container

Si può utilizzare il comando:

```bash
docker run hello-world
```

Come output si ottiene:

![Hello world](./images/docker-hello-world.png)



# Ulteriori informazioni sul sistema

Maggiori informazioni sulla piattaforma si possono ottenere con:

```bash
docker info
```

# Docker vs Virtual Machine

Si tratta di due tecnologie diverse, anche se in prima approssimazione potrebbero essere viste come simili.

Le Virtual Machine rappresentano un'astrazione dell'hardware fisico.

Grazie all'Hypervisor, un server fisico diventa un insieme di server virtuali. Ogni VM ha una copia completa del sistema operativo, librerie, applicazioni. Si tratta quindi di un componente «pesante» rispetto alle risorse utilizzate ed ai tempi di avvio.

I Container sono un'astrazione del sistema operativo. 

Docker e' un processo, in esecuzione su una macchina host con cui condivide il kernel. Sfrutta le funzionalità fornite dal sistema operativo per funzionare come una sandbox.

Nello stesso host Docker, quindi, possono essere in esecuzione molteplici container. Tutti condividono lo stesso kernel, pur rimanendo processi isolati. Rispetto alle VM i container sono componenti più «leggeri» ( minor spazio su file system e minori risorse utilizzate ).


![Confronto Docker vs VM](./images/docker_vs_vm.png)


Le Virtual Machine erogano tipicamente più servizi, un container uno soltanto.



[Sezione gestione Immagini](./exercise1.0.md).