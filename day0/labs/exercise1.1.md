# Docker layer

## The layered filesystem

Un'immagine è il risultato di una composizione di molteplici layer, organizzati gerarchicamente. 
Ognuno contenente il proprio insieme di file e cartelle.

![](./images/docker-image-layer.png)

Il layer più basso viene definito layer base.

Il contenuto dei layer successivi, è il delta rispetto al layer sottostante. 
Questo permette di ottimizzare l'occupazione di spazio. 

Docker sfrutta poi la funzionalità `union filesystem` per creare un filesystem virtuale che risulta la "somma" di tutti i layer presenti.

![Overlay](./images/docker_overlay_constructs.png)

I vari layer di una immagine non sono modificabili ( RO ). Questo ne permette la condivisione ed il riutilizzo.

Un esempio di uno schema ( semplificato ) per un'immagine di tomcat, si compone di:

![](./images/docker-tomcat-image.png)

Il layer base è una Ubuntu. Il layer successivo è rappresentato dal JDK, infine c'è il layer dell'application server.

Come esercizio:

- scaricare prima l'immagine del jdk (``` docker pull openjdk:8-jdk-alpine ```) 
- successivamente quella di maven (``` docker pull maven:3.6.0-jdk-8-alpine ```)

Come deve essere intesa la dicitura "Already exists" che compare durante la pull dell'immagine ?


[Aggiunta di tag alle immagini](./exercise1.2.md)



