# Inspecting containers

In modo analogo a quanto visto per le immagini, il comando inspect si può utilizzare anche per i container:

```bash
docker inspect <CONTAINER_ID> 
```
Ad esempio:

```bash
docker inspect quotes
```

Come output si ottiene un JSON, simile a:



```bash

[
        {
            "Id": "c5c1c68c87...",
            "Created": "2017-12-30T11:55:51.223271182Z",
            "Path": "/bin/sh",
            "Args": [
                "-c",
                "while :; do printf 'The output of command date is: ';date; sleep 5; done"
            ],
            "State": {
                "Status": "running",
                "Running": true,
                ...
            },
            "Image": "sha256:e21c333399e0...",
            ...
            "Mounts": [],
            "Config": {
                "Hostname": "c5c1c68c87dd",
                "Domainname": "",
                ...
            },
            "NetworkSettings": {
                "Bridge": "",
                "SandboxID": "2fd6c43b6fe5...",
                ...
            }
        }
    ]


```

Le informazioni che si possono recuperare sono ad esempio:


- ID del container;

- Timestamp di creazione;

- Immagine base

- Mounts

- Network Settings



Il contenuto può essere filtrato con l'opzione `-f` or `--filter`


```bash

docker inspect -f "{{json .State}}" quotes | jq

docker inspect -f "{{json .NetworkSettings.Networks}}" quotes | jq

```


Inoltre in questo caso viene utilizzato anche il tool jq per migliorare la leggibilità:

```bash

  {
      "Status": "running",
      "Running": true,
      "Paused": false,
      "Restarting": false,
      "OOMKilled": false,
      "Dead": false,
      "Pid": 6759,
      "ExitCode": 0,
      "Error": "",
      "StartedAt": "2017-12-31T10:31:51.893299997Z",
      "FinishedAt": "0001-01-01T00:00:00Z"
  }

```

# Cenni di Network

Quando si installa Docker, vengono create tre network in modo automatico. Si possono vedere con il comando:

```bash
docker network ls

NETWORK ID          NAME                DRIVER
7fca4eb8c647        bridge              bridge
9f904ee27bf5        none                null
cf03ee007fb4        host                host

```
- bridge è la rete di default, dove vengono attestati i container in assenza di ulteriori indicazioni.
- none indica nessuna rete, serve per isolare i container in modo completo
- host è una modalità particolare, permette di condividere la rete del sistema host con il container stesso.

Oltre a queste tipologie di network, quando si utilizza la modalità Swarm, ci sarà a disposizione il driver overlay, che permette la comunicazione tra container su host diversi.

Come esempio, utilizzare due shell diverse per mandare in esecuzione due container a partire dall'immagine sequenceiq/alpine-curl:

```bash
docker run -it --name container1 sequenceiq/alpine-curl /bin/sh
docker run -it --name container2 sequenceiq/alpine-curl /bin/sh
```

Possiamo notare che ogni container ha un suo IP e che i due container si pingano tra loro:

```bash

ip a

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
13: eth0@if14: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.3/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
       
ping <INDIRIZZO IP CONTAINER>

```

Inoltre, ogni container ha un proprio hostname, che coincide con l'identificativo del container stesso:

```bash

hostname
b8d4531f7414

docker ps
CONTAINER ID        IMAGE                    COMMAND             CREATED             STATUS              PORTS               NAMES
b8d4531f7414        sequenceiq/alpine-curl   "/bin/sh"           8 minutes ago       Up 8 minutes                            container2
4b3dd77cd371        sequenceiq/alpine-curl   "/bin/sh"           9 minutes ago       Up 9 minutes                            container1

```

Le informazioni a livello di network si ottengono con una operazione di inspect:

```bash

docker network inspect bridge
 
```
che ritorna le informazioni relative alla rete ed ai container attestati in essa.

## Creazione di network

E' possibile creare delle proprie network con il seguente comando:

```bash
docker network create my-network
```

E mandare in esecuzione dei container sulla network appena creata:

```bash
docker run -it --name container3 --network my-network sequenceiq/alpine-curl /bin/sh
```
Il container3 non è in grado di pingare i container 1 e 2, in quanto attestati su reti diverse.

Quindi la configurazione del network permette di isolare o far comunicare i container tra loro.

## Disconnessione e connessione a network esistenti

La disconnessione si effettua con:

```bash
docker network disconnect <NETWORK> <CONTAINER>

Ad es:
docker network disconnect my-network container3

```
La connessione si effettua con:

```bash
docker network connect <NETWORK> <CONTAINER>

Ad es:
docker network connect bridge container3

```
In questo esempio abbiamo collegato il container3 alla rete bridge, mentre inizialmente era su una rete separata. 

## Rimozione Network

Si effettua con :

```bash
docker network rm <NETWORK>
```

## Link tra Container

La modalità link è deprecata, viene citata per dare una panoramica completa sulla tematica. Si può vedere con il seguente esempio.

Mandare in esecuzione un container con il nome tomcat, come visto negli esempi precedenti.

Eseguire a questo punto:

```bash

docker run -it --link tomcat sequenceiq/alpine-curl /bin/sh

```


Verificare che è possibile effettuare:

```bash

ping tomcat

curl http://tomcat:8080


```

Questo esempio mostra come Docker gestisca automaticamente la risoluzione dei nomi all'interno della piattaforma, permettendo di utilizzare il nome del link come hostname.
Questo aspetto verrà ripreso nella sezione Swarm.

## DNS Interno

Quando si utilizza una user-defined network, quindi una propria rete in modalità bridge, Docker provvede a mappare i nomi dei container su un DNS interno.
Quindi risulta possibile risolvere i nomi all'interno della stessa rete.

In altre parole, è questa la modalità preferibile per ottenere lo stesso comportamento che si ha con la modalità link.




[Exec, Port Mapping](./exercise2.4.md).