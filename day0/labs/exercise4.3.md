# Volumi definiti a livello di Dockerfile

Un volume può essere dichiarato anche a livello di Dockerfile, durante la fase di build.
Questo verrà poi utilizzato all'avvio del container.

Ad esempio si può utilizzare l'immagine MySql, presente su Docker Hub, dopo aver cancellato i volumi presenti sul proprio sistema:

```bash
    docker run --name my-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:5.7

```

Cosa si nota dopo l'avvio del container, eseguendo un listing dei volumi ?



 


[Esercizi Riassuntivi](./exercise4.4.md).




