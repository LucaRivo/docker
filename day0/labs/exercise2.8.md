# Gestione delle Risorse


Alcune direttive previste da Docker, permettono di gestire la quantita' delle risorse, in termini di consumo di CPU e Memoria.

Alcuni esempi pratici permettono di capirne l'utilizzo.


Preparazione ambiente di test: aprire due CLI e in una digitare il comando che permette di visualizzare l'utilizzo di risorse:

```bash

docker stats

```

Occorre inoltre scaricare l'immagine progrium/stress

```bash

docker pull progrium/stress

```

Esercizio 1: Eseguire il seguente container e controllare il consumo delle risorse.

```bash

docker run --rm -d --name all progrium/stress --cpu 1 --timeout 60

```

Esercizio 2: Rieseguire il test utilizzando due istanze:

```bash

docker run --rm -d --name half1 progrium/stress --cpu 1 --timeout 60
docker run --rm -d --name half2 progrium/stress --cpu 1 --timeout 60

```

# Cpu Shares

Tramite l'opzione -c, --cpu-shares possiamo attribuire un peso a ciascun container. Il valore di default vale 1024. Modificando questo parametro possiamo attribuire pesi diversi alle istanze, utilizzati per distribuire la CPU tra i container. Da notare che questo meccanismo viene attivato solo in caso di contesa della risorsa. 

Esercizio 3:

```bash

docker run --rm -d --name peso768 --cpu-shares="768" progrium/stress --cpu 1 --timeout 60
docker run --rm -d --name peso256 --cpu-shares="256" progrium/stress --cpu 1 --timeout 90

```

# Cpu Quota

Le opzioni --cpu-period e --cpu-quota permettono invece di stabilire un massimo consumo di cpu, secondo la seguente formula:
per ogni cpu-period ( in microsecondi ) e' possibile utilizzare solo la "quota" in microsecondi del tempo di CPU.

Esercizio 4:

```bash

docker run --rm -d --name test --cpu-period=60000 --cpu-quota=30000 progrium/stress --cpu 1  --timeout 60

```

# Limite utilizzo di memoria

Ci sono due modalita' con cui viene limitata la memoria: hard e soft.

La prima e' tassativa, se il container supera il valore indicato viene eseguita una kill.

Il secondo e' a "Best effort". Quindi nel caso il container superi la soglia, il funzionamento e' permesso ma non garantito.

Nel primo caso so puo' limitare l'utilizzo di memoria con l'opzione ``` -m ``` seguita dall'unita' di misura ( k, m, g).

Nella seconda modalita' con ```--memory-reservation="<VALORE>" ``` ad esempio ```--memory-reservation="512m" ```


Esercizio 5:

Utilizzare il container progrium/stress , per verificare questa funzionalita'.

Esecuzione di un container senza limitazione ( ogni vm occupa circa 256M di RAM ):

```bash

docker run --rm -d --name test  progrium/stress --vm 4 --timeout 30s

```

Successivamente, imporre una limitazione, ad esempio con:

```bash

docker run -d --name test-hlimit  -m 512m progrium/stress --vm 4 --timeout 20s

```
e controllare poi lo stato con:

```bash

docker inspect test-hlimit -f '{{json .State}}'

```



[Data Management](./exercise4.0.md)


