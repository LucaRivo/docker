# Bind Mounts

Tramite un bind mount � possibile montare files o directory del file system dell�host, all�interno di un container. Non si pu� utilizzare una specifica operazione di creazione come nel caso dei volumi, ma si deve utilizzare l�opzione �v o --mount all�avvio del container:


```bash

docker run [OPTIONS] -v LOCAL_DIR:CONTAINER_MOUNT_POINT IMAGE_NAME [COMMAND]

```

Anche in questo caso � possibile utilizzare l'opzione alternativa --mount

```bash

docker run [OPTIONS] --mount type=bind,source=<LOCAL_DIR>,target=CONTAINER_MOUNT_POINT \
  IMAGE_NAME [COMMAND]

```

Come nel caso dei volumi, una operazione di inspect a livello di container pu� verificare la corretta configurazione:

```bash

docker inspect <ID_CONTAINER>

```

## Esempio di bind mount

Eseguendo:

```bash

docker run -it busybox /bin/sh

```

viene eseguita la shell sh di un'istanza di un container basato su busybox. Supponiamo di voler montare la cartella /tmp dell'host all'interno del container. Possiamo utilizzare un bind mount:

```bash

docker run -it --mount type=bind,source=/tmp,target=/tmp2 \
  busybox /bin/sh

```

Eseguendo una "ls" si vedra' montata la directory tmp2, oltre che la tmp, derivante dall'immagine di busybox.


## Mount di un volume vuoto

Per quanto riguarda i volumi, � stato visto che montando un volume vuoto, viene copiato il contenuto del container all'interno del volume. Il bind mount ha invece un comportamento diverso.

Come prova:

- creare una cartella vuota, ad esempio /tmp/applicazione
- montare questa cartella sulla cartella webapps di Tomcat:

```bash

docker run -d --name=tomcat8 -p 8085:8080 --mount type=bind,source=/tmp/applicazione,target=/usr/local/tomcat/webapps tomcat:8

```

In questo caso non viene copiato il contenuto ma viene fatto l'override dei files.

## Mount di un singolo file

E' possibile effettuare un bind mount di un singolo file.

Come prova:

- nella cartella creata in precedenza, /tmp/applicazione creare un file, ad esempio pippo.html
- montare questo file all'interno dell'applicazione docs di Tomcat:

```bash

docker run -d --name=tomcat8 -p 8085:8080 --mount type=bind,source=/tmp/test/pippo.html,target=/usr/local/tomcat/webapps/docs/pippo.html tomcat:8

```

Cosa succede in questo caso ?
 
 

[Volume in Dockerfile](./exercise4.3.md)




















