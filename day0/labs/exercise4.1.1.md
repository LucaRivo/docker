# Esempi utilizzo Volumi

Esempio di sintassi utilizzo di un volume:

```bash

docker run -d --name tomcat8 -p 8080:8080 \
--mount source=my_webapps,target=/usr/local/tomcat/webapps \
tomcat:8

```


## Persistenza dati

Come esempio, creare un volume (my_logs) da utilizzare per la persistenza dei log di tomcat ( contenuti in /usr/local/tomcat/logs ) e verificare che i log non vengono cancellati anche a seguito di restart o rimozione del container.

Un volume pu� anche essere dichiarato ```readonly```. Questo si ottiene aggiungendo il parametro ,readonly:

```bash

docker run ........ \
--mount source=<SOURCE>,<TARGET>,readonly \
<IMAGE>

```

Cosa succede se si tenta di montare un volume readonly per i log ?

## Mount di un volume vuoto

Il comportamento del mount e' differente nel caso in cui il mountpoint sia vuoto o meno.

Per testare il funzionamenti, seguire la seguente traccia:

- partire da una situazione pulita, cancellando i volumi creati in precedenza
- creare un volume my_webapps e controllare il contenuto prima dell'avvio del container
- avviare il container con l'utilizzo del mount
- controllare il contenuto del volume, dopo l'avvio del container


Se si esegue un container che utilizza o crea un nuovo volume, ed il container contiene files e cartelle nella directory target, il contenuto viene copiato all'interno del volume stesso. 

## Mount di un volume non vuoto

Nel caso in cui venga effettuato il mount di un volume non vuoto, non viene copiato il contenuto dalla cartella target al mountpoint.

Come esempio:

- modificare il file index.jsp dentro alla webapp ROOT, sostituendo ad esempio la stringa Congratulations con Congratulazioni
- cancellare il container e rieseguirlo utilizzando lo stesso volume

## Condivisione del contenuto di un volume

Traccia per verificare questo comportamento.

- Lasciare attivo il container tomcat8
- Avviare un container tomcat9 basato sull'immagine tomcat:9 , notando che nella home page, la versione indicata ad inizio pagina e quella relative alla documentazione siano le stesse
- Eliminare il container con tomcat:9 e mandarlo in esecuzione, questa volta con l'utilizzo dello stesso mount my_webapps
- Verificare la condivisione dei dati ( cartella webapps )

## Utilizzo di un Data Container

In precedenza si � evidenziato come l�operazione di prune cancelli tutti i volumi che non siano referenziati da un container.  
Questo potrebbe causare problemi dovuti all�eliminazione involontaria di volumi necessari per il corretto funzionamento delle applicazioni.

Un modo per ovviare a questo � creare un container, chiamato Data Container, con lo scopo di tenere �agganciato� il volume.

Esempio:

```bash

docker volume create logs_data

docker create --name data_container -v logs_data:/usr/local/tomcat/logs busybox

docker run -d --name tomcat8 --volumes-from data_container -p 8080:8080 tomcat:8


```

[Back to Volumi](./exercise4.1.md)













