# Data Management


Le varie modifiche al file system di un container running, vengono salvate nel Container Layer. Questo implica che tutte le modifiche vengono perse nel momento in cui il container viene riavviato.

Per ottenere la persistenza dei dati , Docker mette a disposizione due opzioni:

- volumes
- bind mount

Inoltre, nel sistemi Linux, � presente anche un meccanismo di tmpfs mounts, che viene per� utilizzato per scopi diversi.

## Tipologie di Mount

![Tipologie di Mount](./images/docker-types-of-mounts.png)  

I volumi sono il metodo consigliato per la persistenza dei dati. Un volume � un�area del file system, gestita da Docker, montata all�interno di uno o pi� container. 

La parte di interazione con lo storage � demandato ad un volume driver. Tramite questi componenti ed i relativi plugins � possibile integrarsi con sistemi esterni ( ad es. con NFS ) oppure utilizzare il cloud.

Un bind mount, invece, permette di montare all�interno del container uno specifico file od una directory del server host. 

Tramite l'utilizzo dei volumi � possible:

-	La condivisione di dati tra container
-	Persistenza dei dati
-	La semplificazione delle operazioni di backup/restore

I bind mount permettono invece di:

-	Condividere files tra la macchina host ed il container

Utilizzando invece un tmpfs mount viene effettuato un mount temporaneo in memoria. Questo non garantisce ovviamente la persistenza del dato, ma viene piuttosto utilizzato per la memorizzazione di dati sensibili o di informazioni non persistenti.

[Volumi](./exercise4.1.md)











