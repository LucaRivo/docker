# Mapping delle porte

Di default i container possono connettersi verso servizi esterni, ma non vale il viceversa. 

Per poter quindi utilizzare i servizi esposti dai container, è necessario effettuare un forward del traffico, ovvero mappare porte dell’host e porte esposte a livello di container.

![Docker Port Mapping](./images/docker-port-mapping.png)

```docker run -d --name tomcat -p 8888:8080 tomcat:8```

Si possono esporre più anche più porte , aggiungendo altre opzioni –p

```docker run -d --name tomcat -p 8888:8080 –p 8005:8005 tomcat:8```

nel caso si debba utilizzare il protocollo UDP la sintassi è:

```-p 53160:53160 -p 53160:53160/udp```

Da notare che, anche avendo più istanze attive, non occorre modificare le porte di ascolto lato container. Basta mappare, a runtime, la stessa porta ( 8080 ) su porte esterne diverse.

Di default viene eseguito un bind su tutti gli indirizzi ( 0.0.0.0 ) ma posso limitare, ad esempio, solo a connessioni da localhost:

```bash

docker run --name tomcat-local-access -d -p 127.0.0.1:8081:8080 tomcat:8

```


# Exec into a running container

Una operazione frequente, specie in fase di debug, è l'esecuzione di comandi all'interno di un container running.

Per questo si può utilizzare:

```bash

docker exec [OPTIONS] ID_CONTAINER COMMAND [ARG...]

```

Ad esempio:

```bash

docker container exec -i -t quotes /bin/sh 

```
Le opzioni -i , -t sono le medesime del comando run.

In questo caso, eseguendo `/bin/sh` si apre una shell all'interno del container, che possiamo utilizzare per dare comandi, eseguire controlli,....

Ad esempio:

```bash

ps

```

produce un output simile a:

![](./images/container-exec-output.png)



# Copia di files tra Host e Container ed utilizzo di diff

Il comando cp permette di copiare files tra host e container:

```bash

docker cp <containerId>:/file/path/within/container /host/path/target

docker cp  /host/path/target <containerId>:/file/path/within/container

```

Come esempio, "deployare" la jsp env.jsp ( presente nella cartella samples ) all'interno di un container Tomcat. Ad esempio nel contesto "examples".

Successivamente si può utilizzare il comando 

```bash

docker diff  <containerId>

```

che elenca i files/directory che sono stati cambiati da quando il container e' stato creato.

La sintassi dell'output e':

|Simbolo|Descrizione|
|:-----|:----------|
|A| A file or directory was added |
|D| A file or directory was deleted |
|C| A file or directory was changed |


# Operazione di commit

L'operazione di commit permette di creare una nuova immagine a partire dal container.

Ovviamente non e' il modo per creare immagini, la cui build deve avvenire tramite Dockerfile, ma puo' essere utilizzato per operazioni di debug.

La sintassi e':

```bash

docker commit  <containerId> <IMAGE_NAME>:[TAG]

```


# Environment Variables

Ad un container si possono passare delle variabili di ambiente. Questo meccanismo può essere sfruttato per effettuare operazioni di configurazione.

L’opzione da utilizzare in questo caso è:

```docker run -e MIA_VARIABILE=”pippo” –e MIA_VARIABILE_2=”123” …```

Le variabili sono visibili anche eseguendo una inspect del container.

Esercizio: 

Per verificare l'utilizzo delle variabili di ambiente, eseguire un container Tomcat utilizzando le opzioni precedenti.
Eseguire poi un comando ENV all'interno del container, oppure utilizzare l'immagine con la env.jsp, vista in precedenza.

Cosa succede se si utilizza una variabile già presente tra quelle definite a livello di immagine ?




# Restarting policy

Docker permette l'utilizzo di specifiche policy di restart. 
Utilizzando il flag --restart alla creazione del container, si puo' scegliere tra:

- Nessuna restart (default)
- Effettuare la restart sempre ( opzione --restart always )
- Restart in caso di failure, ovvero quando il container esce con exit status diverso da zero ( opzione --restart on-failure[:max-retries] )

Come esempio si può utilizzare il container busybox, ad esempio con il comando date:

```docker run -it --rm --name test-restart busybox date```

Si puo' impostare la restart policy su always con:

```docker run -d --name test-restart --restart always busybox date```

E controllare cosa succede analizzando il log.


[Controllo Utilizzo Risorse](./exercise2.8.md).
