# Docker Container


## Introduzione

Un'immagine e' quindi un insieme di layer che definiscono un file system virtuale.

Cosa succede quando si esegue il comando ```docker run``` indicando come parametro una specifica immagine ?

Viene lanciato un processo, che "vede" il file system dell'immagine, secondo il meccanismo di union file system, visto in precedenza. 
Con l'aggiunta di un layer modificabile ( RW ), in cima allo stack, che viene chiamato container layer.

Tutte le modifiche effettuate , come scrittura di nuovi files, editing, cancellazioni vengono riportate su questo layer ( non persistente ).

![Docker Container Layer](./images/docker-container-layers.jpg)

Questo meccamismo permette di avere più container in esecuzione, che condividono lo stesso strato di layer non modificabili.

![Docker Sharing Layer](./images/docker-sharing-layers.jpg)

## Esecuzione di un container

Il comando per l'esecuzione di un comando in un container è:

```docker run [OPTIONS] IMAGE [COMMAND] [ARG...]```

Ad esempio:

![](./images/docker-container-run-description.png)



Come altro esempio, per effettuare un ```ls``` all'interno del container, si utilizza:

```docker run alpine ls -la```

Nel caso non venga specificato un comando, viene lanciato quanto configurato durante la creazione dell'immagine. Ricavabile anche tramite inspect.

Quindi ad esempio con:

```docker run -it tomcat:8```

Come opzioni viene qui specificata la modalità interattiva ( parametro “i” ) con cui vengono agganciati sia lo standard input ed output.  
L’interruzione ( CTRL C ) del processo implica anche la terminazione del container. Per evitare l’interruzione si deve utilizzare CTRL P + CTRL Q


Una modalità alternativa è utilizzare la modalità in background ( detach mode ) con l’opzione –d:

```docker run –d tomcat:8```

Queste modalità comunque non influenzano la funzionalità del container. Dal punto di vista di Docker, l'esecuzione non viene cambiata in base al fatto che ci sia agganciata la console o meno.

Il comando docker run ritorna l'identificativo del container, nel formato esteso. Questo ID viene poi utilizzato come parametro per operazioni di gestione ( start, stop,... ) . In alternativa, si può utilizzare lo short ID ( primi 12 caratteri dell’ID ) oppure il nome del container.

Ad esempio per controllare il log del container si può utilizzare il comando:

```docker logs –f IDCONTAINER```


## Listing containers

La lista dei container si effettua con:

```docker ps [OPTIONS]```

L'output è qualcosa di simile:

![](./images/docker-container-ls.png)


|Column|Description|
|:-----|:----------|
|Container ID| ID univoco del container ( SHA-256 )|
|Image| Nome dell'immagine|
|Command| Comando eseguito all'interno del container|
|Created| Informazione relativa alla creazione del container |
|Status| Stato del Container |
|Ports| Info sulle porte mappate tra host e container |
|Names| Nome del container|

Nel caso si vogliano visualizzare anche i container non attivi:

```bash

docker ps -a 

```

Nella colonna NAMES viene visualizzato il nome del container. Se non viene specificato un nome, docker ne assegna uno arbitrariamente. 
Può essere utile assegnare un proprio nome al container ( opzione - - name all’avvio ) oppure si può rinominare un container con:

```docker rename OLD_NAME NEW_NAME```

## Operazioni di start / stop

Un container running può essere stoppato.

Come esempio, si può mandare in esecuzione questo container:


```bash

docker container run -d --name quotes alpine /bin/sh -c "while :; do printf 'Date: ';date; sleep 5; done"

```
E controllare l'output prodotto ( in modalità tail, specificata dall'opzione -f ) con :

```bash

docker logs -f quotes 

```

Successivamente, per stopparlo:

```bash

docker stop quotes

```

Si può notare che lo stop non è imemdiato, ma occorrono circa 10 secondi.

Questo perchè Docker invia al SO un segnale **SIGTERM**. Nel caso il processo all'interno del container non recepisca il comando, Docker dopo 10sec invia un **SIGKILL**.

Nell'esempio precedente è stato utilizzato il nome del container come identificativo.
In alternativa è possibile utilizzare anche il suo ID.


Un container stoppato può essere fatto ripartire con il comando start:

```bash

docker container start quotes

```

## Comando log

Per ottenere lo STOUT di un container è stato utilizzato in precedenza il comando docker log.

Varie opzioni possono essere utilizzate in abbinata a questo comando.

Un comportamento simile al tail si ottiene con l'opzione -f:

```bash

docker logs -f quotes 

```

Si può inserire un timestamp generato da Docker:

```bash

docker logs -f -t quotes 

```

Si può specificare un intervallo di tempo, ad esempio solo l'ultimo minuto:

```bash

docker logs -f -t --since 1m quotes

```


## Rimozione dei containers

Eseguendo `docker ps -a` tipicamente si possono vedere una serie di container nello stato `Exited`.

Si possono cancellare, liberando risorse nel caso i container non debbano essere più riutilizzati:

The command to remove a container is:

```bash

docker rm <container ID> 

```
**La rimozione non è possibile se il container è in stato running.**
Per forzare la cancellazione si può utilizzare l'opzione `-f` o `--force`.

Per rimuovere tutti i container stoppati:

```bash

docker container prune 

```

## Lifecycle

Durante gli esempi precedenti si è visto che un container può trovarsi in diversi stati.
I vari stati si possono schematizzare in questo modo:

![Docker Container Lifecycle](./images/docker-container-lifecycle.png)



[Operazioni di Inspect](./exercise2.3.md).