# Volumi


## Creazione di un volume

Un volume pu� essere creato tramite CLI con il comando:

```bash

docker volume create <volume_name>

```

ad esempio:

```bash

docker volume create my_webapps

```

In questo caso � stato settatto il nome del volume "my_webapps". 
Nel caso invece in cui non venga specificato, Docker provvede ad attribuire un ID autogenerato.


La lista dei volumi creati di ottiene con:

```bash

docker volume ls

```

I dettagli del volume si ottengono con:

```bash

docker inspect <NOME_VOLUME>

```

dove il valore del Mountpoint indica la cartella del filesystem locale che � stata montata a livello di container.

All'atto della creazione del volume, il contenuto del Mountpoint � vuoto.



## Rimozione di un volume

Per la rimozione del volume si pu� utilizzare il comando:

```bash

docker volume rm <NOME VOLUME>

```

L�operazione pu� essere effettuata solo se il volume non � referenziato da un container. 


La cancellazione massiva di tutti i volumi, pu� essere effettuata con:

```bash

docker volume prune

```


## Utilizzo di un volume da parte di un container

Tra i parametri di avvio di un container, per l�utilizzo di volumi si possono specificare le opzioni ```�v``` oppure ```--mount```.  

Le due opzioni sono equivalenti. Inizialmente il �v era utilizzato per Docker standalone mentre --mount per Swarm (orchestratore di container). Dalla versione Docker 17.06 in poi,  la sintassi � stata uniformata ed il mount pu� essere utilizzato anche per Docker standalone. 
La maggior parte della documentazione ed esempi che si trovano online utilizza l�opzione �v

Di seguito viene riportato lo stesso comando nelle due versioni.

```bash

docker run -d --name tomcat -p 8080:8080 \
-v my_webapps:/usr/local/tomcat/webapps  tomcat:8

```

```bash

docker run -d --name tomcat -p 8080:8080 \
--mount source=my_webapps,target=/usr/local/tomcat/webapps \
tomcat:8

```

In entrambi i casi viene montato il volume my_webapps sul percorso /usr/local/tomcat/webapps.
Questo percorso � quello standard di Tomcat per il deploy delle applicazioni. Ogni applicazione in formato war copiata in questa cartella, viene automaticamente deployata.

Nel caso in cui il volume non sia stato creato in precedenza, viene creato da Docker.

Per la verifica del corretto settaggio del mount si pu� provare una inspect sul container:

```bash

docker inspect tomcat
 
```

Dopo l'avvio del container, il contenuto del Mountpoint � ancora vuoto ?

### Volumi Readonly

Un volume pu� anche essere dichiarato ```readonly```. Questo si ottiene aggiungendo il parametro ,readonly:

```bash

docker run ........ \
--mount source=<SOURCE>,<TARGET>,readonly \
<IMAGE>

```


## Mount di un volume non vuoto

Per verificare il comportamento nel caso di un Mount di un volume non vuoto:

- modificare il file index.jsp dentro alla webapp ROOT, sostituendo ad esempio la stringa Congratulations con Congratulazioni
- cancellare il container e rieseguirlo utilizzando lo stesso volume


Il comportamento del mount e' differente nel caso in cui il mountpoint sia vuoto o meno

## Use Case condivisione di dati tra container

Come esempio, sfruttiamo il volume utilizzato al punto precedente per testare la condivisione di contenuti tra container.

- Lasciare attivo il container tomcat8
- Avviare un container tomcat9 basato sull'immagine tomcat:9 , notando che nella home page, la versione indicata ad inizio pagina e quella relative alla documentazione siano le stesse
- Eliminare il container con tomcat:9 e mandarlo in esecuzione, questa volta con l'utilizzo dello stesso mount my_webapps
- Verificare la condivisione dei dati ( cartella webapps )

## Use Case, persistenza dati 

Come esempio, creare un volume (my_logs) da utilizzare per la persistenza dei log di tomcat ( contenuti in /usr/local/tomcat/logs ) e verificare che i log non vengono cancellati anche a seguito di restart o rimozione del container.


## Use Case Backup e Restore


Il backup di un volume si effettua copiando il contenuto della cartella "_data" presente all'interno del volume.

Ad esempio si pu� effettuare posizionandosi su:

```bash

cd /var/lib/docker/volumes/my_webapps/_data

```

ed invocando il comando :   

```bash

tar cvf /tmp/backup.tar .

```

A questo punto cancellare il container ed il relativo volume, cerare un nuovo volume ed effettuare una restore dalla copia effettuata:

```bash

tar xvf /tmp/backup.tar �C <PATH_DEL_NUOVO_VOLUME>

```

## Pattern Data Container

In precedenza si � evidenziato come l�operazione di prune cancelli tutti i volumi che non siano referenziati da un container.  
Questo potrebbe causare problemi dovuti all�eliminazione involontaria di volumi necessari per il corretto funzionamento delle applicazioni.

Un modo per ovviare a questo � creare un container, chiamato Data Container, con lo scopo di tenere il volume referenziato.


Esempio:

```bash

docker volume create logs_data

docker create --name data_container -v logs_data:/usr/local/tomcat/logs busybox

docker run -d --name tomcat8 --volumes-from data_container -p 8080:8080 tomcat:8


```

[Bind Mounts](./exercise4.2.md)




















