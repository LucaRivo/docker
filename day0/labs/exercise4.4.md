# Esercizi Riassuntivi

## Esercizio 1:

Si utilizzi l’immagine mendhak/udp-listener che implementa un UDP listener. I messaggi ricevuti via UDP vengono scritti su log. La porta di ascolto può essere configurata tramite la variabile di ambiente UDPPORT.

Mandare in esecuzione un container che sia ascolto in UDP sulla porta 7777.

Come client, da utilizzare per un test funzionale, si può utilizzare il comando:

```nc -u localhost UDPPORT```

## Esercizio 2

Si vuole avere a disposizione un sistema di content management, utilizzando il prodotto Wordpress.

Wordpress necessita per il suo funzionamento di un database Mysql. Si vuole inoltre avere un tool per la gestione del database, come ad esempio phpMyadmin.

-------------------------------------------------------

Come prima cosa è utile creare una network apposita per wordpress:

```docker network create wp_network```

Si può passare poi ad installare il database, dopo avere cancellato eventuali volumi per partire da una situazione pulita.
L'immagine Mysql è stata realizzata in modo che la configurazione avvenga tramite variabili di ambiente:
- MYSQL_ROOT_PASSWORD indica la password di root
- MYSQL_DATABASE è il nome del db che si vuole creare
- MYSQL_USER è l'utente proprietario del database sopra indicato
- MYSQL_PASSWORD è la password dell'utente sopra indicato

Quindi ed esempio:

```docker run -d --name=wp_mysql -e MYSQL_ROOT_PASSWORD=12345678 -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress --network wp_network mysql:5.7```

Anche il tool di gestione del database prevede la configurazione tramite variabili di ambiente. 
Per questo esercizio basta la variabile PMA_ARBITRARY=1 che permette il settaggio dei vari parametri direttamente da web interface:

```docker run -d --name wp_dbadmin -e PMA_ARBITRARY=1 --network wp_network -p 8080:80 phpmyadmin/phpmyadmin:4.7```

A questo punto è possibile eseguire il container di Wordpress che necessita di:
- WORDPRESS_DB_HOST per puntare all'indirizzo del database
- WORDPRESS_DB_USER utenza per collegarsi ( di default è root )
- WORDPRESS_DB_PASSWORD per la password
- WORDPRESS_DB_NAME per il nome del database

Tutte le opzioni di possono trovare all'indirizzo: https://hub.docker.com/_/wordpress/

```docker run -d --name wp_server --network wp_network -p 8081:80 -e WORDPRESS_DB_HOST=wp_mysql -e WORDPRESS_DB_PASSWORD=12345678 -e WORDPRESS_DB_NAME=wordpress wordpress:latest```

## Esercizio 3:

Uno sviluppatore necessita di un ambiente con Tomcat versione 8, per provare l'applicazione travel.war ( contenuta nella cartella sample ).
Si desidera quindi, creare una cartella sul filesystem, dove possa copiare l'applicazione e testarla, come se stesse deployando direttamente nella cartella webapps di Tomcat.

## Esercizio 4:

Viene fornito una immagine ( sotto forma di files web_data_container_v1.tar ) contenente pagine web statiche , nel folder /usr/share/nginx/html.
Si desidera pubblicare questo contenuto sul web, tramite un paio di istanze di Web Server.
Si può utilizzare il container nginx ( che di default ha proprio la document root su /usr/share/nginx/html )


# Riassunto Concetti Container

- immagine in esecuzione, contiene le componenti applicative, librerie, dati, risorse grafiche

- veloce, viene eseguito in secondi (al contrario delle normali macchine virtuali)

- ha un proprio indirizzo IP

- condivide i layer RO con le altre istanze attive, utilizza un proprio layer RW non persistente

- espone porte per comunicare con il mondo esterno

- puo' utilizzare variabili di ambiente per la configurazione

- utilizza volumi per la persistenza dei dati

