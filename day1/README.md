#Second day

# Parte 1

Dopo aver visto i comandi di base per l'utilizzo di immagini e container, in questa sezione viene affrontato il tema della build dei container.

## Agenda

1. [Introduzione al Dockerfile](./labs/part-1/exercise0.0.md)
2. Istruzioni per la build
    - [FROM , ARG, ADD/COPY, LABEL](./labs/part-1/exercise1.1.md)
    - [ENV, RUN, CMD, EXPOSE, WORKDIR](./labs/part-1/exercise1.2.md)
    - [ENTRYPOINT, VOLUMES](./labs/part-1/exercise1.3.md)
3. [Best Practice](./labs/part-1/exercise1.3.md)

## Obiettivi

Lo scopo di questa sezione è maturare la conoscenza necessaria per realizzare container custom.

[Introduzione](./labs/part-1/exercise0.0.md).

# Parte 2

Durante le precedenti sessioni di esercitazione abbiamo:

- Compreso che cosa sono i container applicativi e le immagini Docker;

- Come creare immagini Docker per containerizzare un'applicazione;

- Come interagire con il run time Docker per eseguire un'applicazione containerizzata.

## Agenda

Nella presente sessione, invece, approfondiremo i seguenti temi:

1. [Docker Swarm ](./labs/part2-dev/lab0.md);
2. [Comandi base: inizializzazione e gestione nodi](./labs/part2-dev/lab1.md);
3. [Esempio Docker Compose](./labs/part2-dev/lab3.md);
4. [Gestione della configurazione in ambito Container](./labs/part2-dev/lab4.md);
5. [Stack multiservizio](./labs/part2-dev/lab5.md);
6. [Gestione Secrets](./labs/part2-dev/lab6.md);
7. [Scheduling e Running](./labs/part2-dev/lab7.md);
8. [Risorse e Network](./labs/part2-dev/lab8.md);
9. [Overview Conclusiva](./labs/part2-dev/lab9.md);


## Obiettivi

Al termine della sessione sarai in grado di:

- Comprendere la modalità di utilizzo di Docker Swarm;

- Utilizzare lo strumento Docker Compose per
  codificare l'architettura di un'applicazione distribuita e containerizzata.

- Gestione delle configurazioni.

Procedere all'inizio della [tematica Docker Swarm](./labs/part2-dev/lab0.md).