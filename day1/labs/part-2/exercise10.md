# Consumare gli eventi del Sistema Docker

L'engine Docker, durante la creazione, l'esecuzione, l'arresto e
la rimozione di container e altre risorse quali volumi o reti,
produce un registro degli eventi. Questi eventi possono essere
consumati da sistemi esterni, come alcuni servizi di infrastruttura
che li utilizzano per prendere decisioni informate. Un esempio di tale
servizio potrebbe essere uno strumento che crea un inventario di tutti
i container attualmente in esecuzione sul sistema.

Possiamo collegarci a questo flusso di eventi di sistema ed emetterli,
ad esempio in un terminale, usando il seguente comando:

```bash

# Attenzione che il comando è bloccante
docker system events

```

Supponendo di aver eseguito il comando precedente, in una finestra del
terminale aggiuntiva, eseguire un container come questo:

```bash

docker container run --rm alpine echo "Hello World"

```

In output d'esempio segue:

```bash

2018-05-11T12:15:56.214288013+02:00 container create d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95 (image=alpine, name=festive_shtern)
2018-05-11T12:15:56.215824615+02:00 container attach d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95 (image=alpine, name=festive_shtern)
2018-05-11T12:15:56.264821629+02:00 network connect c8e4760780ac46b00fc934b34cec567acfc5026bfd5c75ab3275a0e39a6770b3 (container=d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95, name=bridge, type=bridge)
2018-05-11T12:15:56.565601465+02:00 container start d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95 (image=alpine, name=festive_shtern)
2018-05-11T12:15:56.608703173+02:00 container die d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95 (exitCode=0, image=alpine, name=festive_shtern)
2018-05-11T12:15:56.687821855+02:00 network disconnect c8e4760780ac46b00fc934b34cec567acfc5026bfd5c75ab3275a0e39a6770b3 (container=d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95, name=bridge, type=bridge)
2018-05-11T12:15:56.755428060+02:00 container destroy d810ab6c25e64d656c62c0ab739026750741eec202a829e7468b063d00f5fd95 (image=alpine, name=festive_shtern)

```

In questo output, possiamo seguire l'esatto ciclo di vita del container.
Il container viene creato, avviato e quindi distrutto.

**Esercizio autonomo**:

Rifare i passi esposti sopra con una sola eccezione, la finestra
del terminale che stampa a video gli eventi di sistema deve fermare
il precedente comando `docker system events` e lanciare:

```bash

docker system events --format 'Type={{.Type}} Image={{.Actor.Attributes.image}} Action={{.Action}}'

```

Che cosa stampa a video?

Procedere all'[esercizio 11](./exercise11.md).