# Build e Push di un’applicazione

Per le operazioni di *build* e *push* possiamo utilizzare Docker Compose anche
in fase di sviluppo.

Le due operazioni che ci interessano sono:

- Build:

    ```bash

    docker-compose -f docker-compose.dev.yml build

    ```

- Push:

    ```bash

    docker-compose -f docker-compose.dev.yml push

    ```

**Esercizio autonomo**:

Per l'esercizio utilizzare i file della demo `demo-docker-compose`.
Eseguire le due operazioni sul proprio laptop. In seguito, descrivere i problemi
incontrati.

Procedere all'[esercizio 17](./exercise17.md).