# La rete Bridge

La rete Docker Bridge è la prima implementazione del
modello di rete del container che esamineremo in dettaglio.
Questa implementazione di rete si basa sul bridge Linux.
Quando il daemon Docker viene eseguito per la prima volta,
esso crea un bridge Linux e lo chiama *docker0*. Questo è
il comportamento predefinito e può essere modificato cambiando
la configurazione. Docker crea quindi una rete  e configura
il traffico all'interno di questa. Tutti i container, che
creiamo sul host Docker e che non connettiamo esplicitamente
a un'altra rete, Docker li collega a questa rete Bridge di default.

Per verificare che effettivamente abbiamo una rete chiamata Bridge
sul nostro host, possiamo elencare tutte le reti sull'host
con il seguente comando:

```bash

docker network ls

```

Questo dovrebbe fornire un output simile al seguente:

![](./images/net-ls.png)

Nel tuo caso, gli ID saranno diversi, ma il resto dell'output
dovrebbe essere lo stesso. In effetti, disponiamo di una prima
rete chiamata bridge che utilizza il bridge driver. L'ambito
locale significa che questo tipo di rete è limitato a un
singolo host e non può estendersi su più host.

Ora, vediamo un po 'più in profondità in cosa consiste
questa rete bridge. Per questo,useremo il comando di controllo Docker:

```bash

docker network inspect bridge

```

Quando viene eseguito, questo produce una grande porzione di informazioni
dettagliate sulla rete in questione. Questa informazione dovrebbe
apparire come la seguente:

```bash

[
    {
        "Name": "bridge",
        "Id": "a796813f12f460bec19a60d983be793a9326f07f8f346c48839adfa4b4eef90c",
        "Created": "2018-05-08T16:57:50.479488399+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": null,
            "Config": [
                {
                    "Subnet": "172.17.0.0/16"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {},
        "Options": {
            "com.docker.network.bridge.default_bridge": "true",
            "com.docker.network.bridge.enable_icc": "true",
            "com.docker.network.bridge.enable_ip_masquerade": "true",
            "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
            "com.docker.network.bridge.name": "docker0",
            "com.docker.network.driver.mtu": "1500"
        },
        "Labels": {}
    }
]


```

Avendo osservato l'output del commando `docker network inspect bridge`,
focalizziamoci sul campo **IPAM** (IP Address Management).
IPAM è un applicativo software che è utilizzato internamente
a un computer per tracciare gli indirizzi IP che sono
utilizzati nel computer.

Nel blocco informativo IPAM, la parte interessante è la
sessione *Config* con i valori *Subnet* e *Gateway*.
La subnet per la rete bridge è definita di default a `172.17.0.0/16`.

Questo implica che, ogni container, in esecuzione sul host di Docker,
ottiene un indirizzo IP direttamente da Docker, nel range
`172.17.0.2 - 172.17.255.255`. L'indirizzo IP `172.17.0.1` è dedicato
esclusivamente al *router* di questa rete. Se connettiamo il primo
container alla rete bridge esso riceverà come indirizzo IP da Docker
`172.10.0.2`. Ogni container successivo otterrà un numero IP sempre
crescente. L'immagine che segue illustra quanto detto:

![](./images/net.png)

Avendo visto come la rete funziona, andiamo a creare
qualche rete bridge personalizzata:

```bash

docker network create --driver bridge sample-net

```

Se lo facciamo, possiamo controllare quale subnet Docker
ha creato per questa nuova rete personalizzata come segue:

```bash

docker network inspect sample-net | grep Subnet

```

Il commando ritorna il seguente output:

```bash

"Subnet": "172.18.0.0/16",

```

Evidentemente, Docker ha appena assegnato il prossimo
blocco libero di indirizzi IP alla nostra nuova rete
bridge personalizzata.

Ora che abbiamo discusso di cosa sia una rete bridge e
come si possa creare una rete bridge personalizzata,
vogliamo capire come possiamo connettere i container
a queste reti. Innanzitutto, eseguiamo in modo interattivo
un container Alpine senza specificare la rete da collegare:

```bash

docker container run --name c1 -it --rm alpine:latest /bin/sh

```

In un'altra finestra di Terminale, ispezioniamo il container c1:

```bash

docker container inspect c1

```

Nel vasto output, concentriamoci per un momento sulla parte che
fornisce informazioni relative alla rete. Può essere trovato
sotto il nodo *NetworkSettings*. Osservare il seguente output:

```bash
...
"NetworkSettings": {
            "Bridge": "",
            "SandboxID": "fad51ef7b95faf010512e7e711ac51c66b036fb91bb3e666b9b5bd7271d46270",
            "HairpinMode": false,
            "LinkLocalIPv6Address": "",
            "LinkLocalIPv6PrefixLen": 0,
            "Ports": {},
            "SandboxKey": "/var/run/docker/netns/fad51ef7b95f",
            "SecondaryIPAddresses": null,
            "SecondaryIPv6Addresses": null,
            "EndpointID": "cbf3a2559c3882887f2ab127a453b6d6c7465dca7808397e08c6bfe27482c4bd",
            "Gateway": "172.17.0.1",
            "GlobalIPv6Address": "",
            "GlobalIPv6PrefixLen": 0,
            "IPAddress": "172.17.0.2",
            "IPPrefixLen": 16,
            "IPv6Gateway": "",
            "MacAddress": "02:42:ac:11:00:02",
            "Networks": {
                "bridge": {
                    "IPAMConfig": null,
                    "Links": null,
                    "Aliases": null,
                    "NetworkID": "a796813f12f460bec19a60d983be793a9326f07f8f346c48839adfa4b4eef90c",
                    "EndpointID": "cbf3a2559c3882887f2ab127a453b6d6c7465dca7808397e08c6bfe27482c4bd",
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
                    "IPPrefixLen": 16,
                    "IPv6Gateway": "",
                    "GlobalIPv6Address": "",
                    "GlobalIPv6PrefixLen": 0,
                    "MacAddress": "02:42:ac:11:00:02",
                    "DriverOpts": null
                }
            }
        }
...

```

Osserviamo che il container c1 è connesso alla rete `bridge`,
ha ricevuto l'indirizzo IP `172.17.0.2`.
Notiamo che il container abbia anche un
indirizzo MAC associato con esso. Questo
è importante in quanto il Bridge Linux utilizza
l'indirizzo MAC per il routing del traffico.

Procediamo ad osservare la rete dall'interno del container:

```bash

ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
8: eth0@if9: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

Il *fun fact* nel precedente output è il numero 19,
l'endpoint `eth0`. Infatti, Docker ha creato un accoppiamento
delle interfacce di rete. Docker ha connesso, automaticamente,
l'interfaccia di rete del host Docker con l'interfaccia di rete
interna al container. Se il container è connesso a più di una
rete, allora l'endpoint sarà collegato anche ad *eth1* ed ecc.

```bash

8: eth0@if9: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever


```

Nell'output, possiamo anche vedere quale indirizzo
MAC (02:42:ac:11:00:02) e quale IP (172.17.0.2) è stato
associato a questo container da Docker.

Possiamo anche ottenere alcune informazioni su come vengono
instradate le richieste utilizzando il comando `ip route`:

```bash
# Per accedere alla shell del container eseguire
# docker exec -it c1 /bin/sh


/ # ip route
default via 172.17.0.1 dev eth0 
172.17.0.0/16 dev eth0 scope link  src 172.17.0.2

```

Questo output ci dice che, tutto il traffico verso il
gateway 172.17.0.1 viene instradato attraverso il dispositivo eth0.

Ora, eseguiamo un altro container chiamato c2 sulla stessa rete:

```bash

docker container run --name c2 -d alpine:latest ping 127.0.0.1

```

Il container c2 verrà anche esso collegato alla rete bridge, poiché
non abbiamo specificato alcuna altra rete. Il suo indirizzo IP sarà
quello successivo e libero della subnet, 172.17.0.3,
come possiamo facilmente testare:

```bash

docker container inspect --format "{{.NetworkSettings.IPAddress}}" c2
172.17.0.3

```

Ora, abbiamo due container collegati alla rete bridge. Possiamo provare a
ispezionare questa rete ancora una volta per trovare un elenco di tutti
i container connessi nell'output:

```bash

"Containers": {
            "0469aa7341994acf8f68d305ce9eebe576b9b528838835436ea09a1d051ea22e": {
                "Name": "c1",
                "EndpointID": "cbf3a2559c3882887f2ab127a453b6d6c7465dca7808397e08c6bfe27482c4bd",
                "MacAddress": "02:42:ac:11:00:02",
                "IPv4Address": "172.17.0.2/16",
                "IPv6Address": ""
            },
            "66fb3de1f0ae6711e752c15a177f3c1887b73ce4c772f96d720c043ca4b65da6": {
                "Name": "c2",
                "EndpointID": "2f21a0b4d1e2236f920a75e6b1fc5323f4611e7849c47f84529b7f807646ee90",
                "MacAddress": "02:42:ac:11:00:03",
                "IPv4Address": "172.17.0.3/16",
                "IPv6Address": ""
            }
        }

```

Ancora una volta, abbiamo ridotto l'output all'essenziale per favorire la leggibilità.

Ora creiamo due container aggiuntivi, c3 e c4, e li colleghiamo alla rete di test.
Per questo, usiamo il parametro `--network`:

```bash

docker container run --name c3 -d --network test-net alpine:latest ping 127.0.0.1

docker container run --name c4 -d --network test-net alpine:latest ping 127.0.0.1

```

Controlliamo la rete `test-net` e confermiamo che i container c3 e c4 sono
effettivamente collegati alla rete:

```bash
docker network inspect test-net
"Containers": {
            "415cdb52371e16b124a99ff8e2772cc4b1da7c58ffe3e6f153a7bc257645122f": {
                "Name": "c4",
                "EndpointID": "8375d69c4a344cdf69aec009f220605ec2ff2a4c5314d33e847ef165baede561",
                "MacAddress": "02:42:ac:12:00:03",
                "IPv4Address": "172.18.0.3/16",
                "IPv6Address": ""
            },
            "f44238e7565debb5ab1001b083c61fc1bdd179ac0266252896a58fe3b79bb74e": {
                "Name": "c3",
                "EndpointID": "2950ce77c6846da90efc613842147477ed5c6aa8769662f303a2683d18ab20dd",
                "MacAddress": "02:42:ac:12:00:02",
                "IPv4Address": "172.18.0.2/16",
                "IPv6Address": ""
            }
        }

```

La prossima domanda che ci chiederemo è se i due container c3 e c4 possono
comunicare liberamente tra di loro. Per dimostrare che questo è davvero il caso, 
possiamo eseguire `l'exec` nel container c3:

```bash

docker container exec -it c3 /bin/sh

```

Una volta all'interno del container, possiamo provare ad effettuare
il ping verso il container c4 per nome e per indirizzo IP:

```bash

ping c4
PING c4 (172.18.0.3): 56 data bytes
64 bytes from 172.18.0.3: seq=0 ttl=64 time=0.196 ms
64 bytes from 172.18.0.3: seq=1 ttl=64 time=0.253 ms
64 bytes from 172.18.0.3: seq=2 ttl=64 time=0.255 ms



 ping 172.18.0.3
PING 172.18.0.3 (172.18.0.3): 56 data bytes
64 bytes from 172.18.0.3: seq=0 ttl=64 time=0.276 ms
64 bytes from 172.18.0.3: seq=1 ttl=64 time=0.090 ms
64 bytes from 172.18.0.3: seq=2 ttl=64 time=0.165 ms

```

La risposta in entrambi i casi ci conferma che la comunicazione
tra i container collegati alla stessa rete funziona come previsto.
Il fatto che possiamo persino usare il nome del container a cui
vogliamo collegarci mostra che la risoluzione dei nomi fornita dal
servizio Docker DNS funziona all'interno di questa rete.

Ora vogliamo essere sicuri che il bridge e le reti `test-net` siano
segregate tra di loro. Per dimostrarlo, possiamo provare ad eseguire
il ping verso il container c2 dal container c3, con il suo nome o
il suo indirizzo IP:

```bash

 ping c2
ping: bad address 'c2'

ping 172.17.0.3
PING 172.17.0.3 (172.17.0.3): 56 data bytes
^C
--- 172.17.0.3 ping statistics ---
2 packets transmitted, 0 packets received, 100% packet loss


```

Le reti forniscono un ulteriore livello di isolamento, e
quindi sicurezza, ai container.

In precedenza, abbiamo appreso che un container può essere
collegato a più reti. Colleghiamo un container c5 alle
reti `sample-net` e `test-net` allo stesso tempo:

```bash

docker container run --name c5 -d \
    --network sample-net \
    --network test-net \
    alpine:latest ping 127.0.0.1

```

**Esercizio autonomo**:

Verificare che il container c5 è raggiungibile su entrambe
le reti (*sample-test*, *test-net*).

Se vogliamo rimuovere una rete esistente, possiamo usare il comando
**rm** di `docker network`.

`N.B. Si noti che non è possibile eliminare una rete a cui sono collegati dei container.`

```bash

docker network rm test-net
Error response from daemon: network test-net id aa98e9ce79a31fb739e4ad70f6352c65d565ccce197ab962d05f000c6c3ec85e has active endpoints

```

Prima di continuare, sistemiamo il contesto:

```bash

docker container rm -f $(docker container ls -aq)

```

Infine, rimuoviamo le due reti personalizzate che abbiamo creato:

```bash

docker network rm sample-net
docker network rm test-nets
```

Procedere all'[esercizio 3](./exercise3.md).