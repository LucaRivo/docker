# Liberare le risorse

Una volta che abbiamo deciso che è necessaria una pulizia, Docker ci
fornisce i relativi comandi di pulizia. Per ogni risorsa, come
immagini, container, volumi e reti esiste un comando per liberare
le risorse (`prune`).

## Pruning dei container

Vogliamo liberare risorse di sistema rimuovendo dei container.

Iniziamo con questo comando:

```bash

docker container prune

```

Il comando precedente rimuoverà tutti i container dal sistema che non
sono in stato di *running*. Docker ci chiederà conferma prima di
eliminare i container attualmente in stato *Exited* o *Created*.

**Domanda**:

E per quanto riguarda i container in stato *Dead*, qualcuno ha qualche idea?

Per saltare il passaggio di conferma possiamo usare il flag `-f` (o `--force`):

```bash

docker container prune -f

```

In determinate circostanze, potremmo voler rimuovere tutti i container dal
nostro sistema, anche quelli in esecuzione.

*Non possiamo usare il comando prune per questo.*

Invece dovremmo usare un comando, come segue:

```bash

docker container rm -f $(docker container ls -aq)

# Oppure

docker ps -aq | xargs docker rm -f

```

## Pruning delle immagini

Se vogliamo liberare lo spazio occupato dai *layer* di un'immagine inutilizzati,
possiamo usare il seguente comando:

```bash

docker image prune

```

Dopo aver confermato che vogliamo effettivamente liberare lo spazio
occupato dai layer di un'immagine inutilizzati, questi vengono rimossi.

Ora dobbiamo specificare cosa intendiamo quando si parla di layer di
un'immagine inutilizzati. Come ricorderai dalla sezione precedente,
un'immagine è composta da una pila di livelli immutabili. Ora, quando
creiamo un'immagine personalizzata più volte, apportando ogni volta alcune
modifiche, ad esempio, al codice sorgente dell'applicazione, per cui
stiamo costruendo l'immagine, ricreiamo i livelli; le versioni precedenti
dello stesso livello diventano orfane . *Perché?*
Il motivo è che i livelli sono immutabili. Pertanto, quando qualcosa
nella sorgente utilizzata per costruire un livello viene modificato,
lo stesso livello deve essere ricostruito e la versione precedente verrà
abbandonata.

Su un sistema in cui spesso avviene il build delle immagini, il numero di livelli
di un'immagine orfani può aumentare notevolmente nel tempo. Tutti questi strati
orfani vengono rimossi con il precedente comando di `prune`.

```bash

docker image prune -f

```

Esiste una versione ancora più radicale del comando `docker image prune`. A
volte non vogliamo solo rimuovere i livelli di immagine orfani ma tutte le
immagini che non sono attualmente in uso sul nostro sistema.
Per questo, possiamo usare un flag:

```bash

docker image prune --force --all

```

## Pruning dei volumi

I volumi Docker vengono utilizzati per consentire al container di  persistere
i dati. Questi dati possono essere importanti e quindi i comandi discussi in
questa sezione dovrebbero essere applicati con particolare attenzione.

Se sai che vuoi recuperare spazio occupato dai volumi e distruggere
irreversibilmente i dati sottostanti, puoi usare il seguente comando:

```bash

docker volume prune

```

Questo comando rimuoverà tutti i volumi che non sono attualmente in
uso da almeno un container.

Un indicatore utile quando si eliminano i volumi è il flag `-f` o
`--filter` che ci consente di specificare l'insieme di volumi che
stiamo considerando per l'eliminazione. Guardiamo il seguente comando:

```bash

# Creamo un volume con un label associato
docker volume create --label demo test

# Filtriamo tutti i volumi, proiettando sulla chiave:valore
docker volume ls --filter 'label=demo'

# L'operazione vera e propria di pruninig
docker volume prune --filter 'label=demo'

```

Questo applicherà il comando solo ai volumi che hanno un'etichetta
con il valore demo. Il formato flag di filtro è `key=value`. Se è
necessario più di un filtro, è possibile utilizzare più flag:

```bash

docker volume prune --filter 'label=demo' --filter 'label=test'

```

## Pruning delle reti

L'ultima risorsa che può essere eliminata sono le reti.
Per rimuovere tutte le reti inutilizzate, usiamo il seguente comando:

```bash

docker network prune

```

Ciò rimuoverà le reti su cui attualmente non è collegato alcun
container o service.

## Pruning totale

Se vogliamo liberare le risorse senza dover immettere più comandi,
possiamo usare il seguente comando:

```bash

docker system prune

```

Il comando sopra chiede una conferma prima di rimuovere rimuove tutti
i container, le immagini, i volumi e le reti inutilizzati in una
volta sola e nell'ordine corretto.

**Domanda**:

Come possiamo liberare le risorse senza dover confermare l'operazione manualmente?

Procedere all'[esercizio 10](./exercise10.md).