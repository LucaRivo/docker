# Scalare orizzontalmente un’applicazione

Ora, supponiamo, per un momento, che la nostra applicazione di esempio
sia stata pubblicata sul Web e abbia avuto molto successo. Un sacco di
persone vogliono vedere le nostre simpatiche GIF.

Quindi ora stiamo affrontando un problema: la nostra applicazione ha iniziato
a rallentare. Per contrastare questo problema, vogliamo eseguire più istanze
del servizio web. Con Docker Compose questo è facile.

L'esecuzione di più istanze viene anche chiamata `scale up`.
Possiamo utilizzare questo strumento per adattare il nostro servizio
web, ad esempio, tre istanze:

```bash

docker-compose up --scale web=3

```

Subito dopo dovresti ricevere un `errore`.

La seconda e la terza istanza del servizio `web` non vengono avviate.
Il messaggio di errore ci dice che: 
*non possiamo usare la stessa porta sullo stesso host più di una volta*. Quando
le istanze 2 e 3 tentano di avviarsi, Docker si rende conto che la porta `3000`
è già stata occupata dalla prima istanza.

Come possiamo ovviare a questo problema?

Dobbiamo seguire i seguenti *step*:

1. Fermare l'applicazione: `docker-compose down`;

2. Modificare il file `docker-compose.yml` nella sezione `ports`  da `3000:3000` a `3000`;

3. Mettere in esecuzione nuovamente l'applicazione `docker-compose up -d`;

4. Se facciamo `docker-compose ps` vedremo i servizi in stato di *running*.

Procedere all'[esercizio 16](./exercise16.md).