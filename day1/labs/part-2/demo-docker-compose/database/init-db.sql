-- create table for images

CREATE TABLE images
(
  imageid serial UNIQUE PRIMARY KEY,
  description character varying(10485760) NOT NULL,
  url character varying(255) NOT NULL
);

ALTER TABLE images
  OWNER TO dockeruser;
ALTER ROLE dockeruser CONNECTION LIMIT -1;

-- add image data
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/mlvseq9yvZhba/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/8vQSQ3cNXuDGo/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/nNxT5qXR02FOM/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/33OrjzUFwkwEg/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/xBAreNGk5DapO/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/l1J3pT7PfLgSRMnFC/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/yFQ0ywscgobJK/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/xJjs8eGVbjNYY/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/xtGpIp4ixR6Gk/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/uTCAwWNtz7U2c/giphy.gif');
INSERT INTO images (description, url) VALUES('cat image', 'https://media.giphy.com/media/p4xp4BjHIdane/giphy.gif');
