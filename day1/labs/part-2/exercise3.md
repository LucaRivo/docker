# La rete Host

Esistono occasioni in cui vogliamo eseguire un container nel *namespace* della
rete dell'host. Questo può essere necessario quando è richiesto eseguire alcuni
container per analizzare o eseguire, per esempio, il debug del traffico della
rete host. Ma tieni presente che si tratta di scenari molto specifici.

Detto questo, come possiamo eseguire un container all'interno del namespace di
rete dell'host? Semplicemente collegando il container alla rete host:

```bash

docker container run --rm -it --network host alpine:latest /bin/sh

```

Se ora usiamo lo strumento `ip` per analizzare lo spazio dei nomi di rete
all'interno del container, vedremo che otterremo esattamente la stessa
immagine che avremmo se eseguissimo lo strumento `ip` direttamente sull'host.
Ad esempio, se ispezioniamo il device `eth0` sul host, otteniamo questo:

```bash
# N.B. Sostituire NET_INTERFACE con la propria interfaccia di rete.
ip addr show ${NET_INTERFACE}

5: enp0s20u2u1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP qlen 1000
    link/ether f0:1e:34:11:bc:fc brd ff:ff:ff:ff:ff:ff
    inet 172.25.22.7/23 brd 172.25.23.255 scope global dynamic enp0s20u2u1
       valid_lft 9170sec preferred_lft 9170sec
    inet6 fe80::8b4b:5dd3:9367:1bb4/64 scope link 
       valid_lft forever preferred_lft forever

```

Possiamo anche ispezionare le rotte per ottenere quanto segue (abbreviato):

```bash

/ # ip route
default via 172.25.22.20 dev enp0s20u2u1  metric 100
172.17.0.0/16 dev docker0 scope link  src 172.17.0.1
172.18.0.0/16 dev br-aa98e9ce79a3 scope link  src 172.18.0.1
172.19.0.0/16 dev br-705cc3ca1d1b scope link  src 172.19.0.1
172.25.22.0/23 dev enp0s20u2u1 scope link  src 172.25.22.7  metric 100
192.168.122.0/24 dev virbr0 scope link  src 192.168.122.1

```

**Domanda**:

Qual è il più grande svantaggio nell'utilizzare la rete *host* in ambito
Docker? Individuare almeno due svantaggi.

Procedere all'[esercizio 4](./exercise4.md).