# Docker Compose

Nelle esercitazioni scorse abbiamo avuto modo di vedere il modello di
rete CNM, gestione ed ottimizzazione dell'utilizzo delle risorse in
contesto Docker e cenni di eventi Docker.

In questa sessione di esercitazione guidata approfondiremo i seguenti *topic*:

- Reinterpretare il dichiarativo verso imperativo;

- Eseguire un’applicazione complessa;

- Scalare orizzontalmente un’applicazione;

- Build e Push di un’applicazione containerizzata.

Al termine di questa sessione sarai in grado di:

- Spiegare in breve le principali differenze tra un approccio imperativo
    e dichiarativo per la definizione e l'esecuzione di un'applicazione;

- Descrivere con parole proprie la differenza tra un container e un servizio
    di Docker Compose;

- Definire il manifesto YAML per una semplice applicazione multi-servizio;

- Build, push, deployment e gestione di una semplice applicazione multi service
    utilizzando il Docker Compose;

- Usare Docker Compose per scalare un servizio applicativo su e giù.

Procedere all'[esercizio 13](./exercise13.md).