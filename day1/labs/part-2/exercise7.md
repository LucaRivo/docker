# Comprendere lo stato del Sistema di Containerizzazione (Docker)

Durante attività di troubleshooting dell'ambiente Docker, i comandi
che presenteremo in questa sessione di esercitazione sono indispensabili.

Introduciamo il comando `docker version`. Esso fornisce un abbondante
insieme di informazioni in merito allo strumento. *Perché?*

Se esegui il comando nella terminale, dovresti vedere qualcosa di simile a
quanto segue:

```bash

Client:
 Version:      18.03.1-ce
 API version:  1.37
 Go version:   go1.9.5
 Git commit:   9ee9f40
 Built:        Thu Apr 26 07:19:31 2018
 OS/Arch:      linux/amd64
 Experimental: false
 Orchestrator: swarm

Server:
 Engine:
  Version:      18.03.1-ce
  API version:  1.37 (minimum version 1.12)
  Go version:   go1.9.5
  Git commit:   9ee9f40
  Built:        Thu Apr 26 07:23:21 2018
  OS/Arch:      linux/amd64
  Experimental: false

```

Nel caso di sopra, vediamo che, sia il *client* sia il *server* utilizzano
la versione `18.03.1-ce Community Edition` del demone Docker.

Per chiarire le idee, prendiamo come riferimento la seguente immagine:

![](./images/client-server-docker.png)

Osserviamo che abbiamo due contesti separati: uno è il proprio laptop e il
secondo è il server remoto.

Con un unico *Client Docker* possiamo gestire i due ambienti separati.

Come possiamo farlo? Dobbiamo configurare le seguenti variabili d'ambiente:

- `DOCKER_HOST`;

- `DOCKER_TLS_VERIFY`;

- `DOCKER_CERT_PATH`.

**Esempio**:

Prendere in considerazione l'esempio fornito dal tutor durante la lezione.

In caso che queste variabili d'ambiente non siano configurate, implica che
il proprio *Client Docker* comunica con il *Server Docker* locale al proprio
ambiente.

Un ulteriore comando importante per attività di troubleshooting è `docker system info`.

Un output d'esempio è come segue:

```bash

Containers: 3
 Running: 0
 Paused: 0
 Stopped: 3
Images: 1
Server Version: 18.03.1-ce
Storage Driver: overlay2
 Backing Filesystem: extfs
 Supports d_type: true
 Native Overlay Diff: true
Logging Driver: json-file
Cgroup Driver: cgroupfs
Plugins:
 Volume: local
 Network: bridge host macvlan null overlay
 Log: awslogs fluentd gcplogs gelf journald json-file logentries splunk syslog
Swarm: active
 NodeID: p61lo6osn1rmpar4svno5hqim
 Is Manager: true
 ClusterID: 7nnspq81xzbubyx7ve2izcs3n
 Managers: 1
 Nodes: 1
 Orchestration:
  Task History Retention Limit: 5
 Raft:
  Snapshot Interval: 10000
  Number of Old Snapshots to Retain: 0
  Heartbeat Tick: 1
  Election Tick: 10
 Dispatcher:
  Heartbeat Period: 5 seconds
 CA Configuration:
  Expiry Duration: 3 months
  Force Rotate: 0
 Autolock Managers: false
 Root Rotation In Progress: false
 Node Address: 172.20.25.242
 Manager Addresses:
  172.20.25.242:2377
Runtimes: runc
Default Runtime: runc
Init Binary: docker-init
containerd version: 773c489c9c1b21a6d78b5c538cd395416ec50f88
runc version: 4fc53a81fb7c994640722ac585fa9ca548971871
init version: 949e6fa
Security Options:
 seccomp
  Profile: default
Kernel Version: 4.16.6-302.fc28.x86_64
Operating System: Fedora 28 (Workstation Edition)
OSType: linux
Architecture: x86_64
CPUs: 4
Total Memory: 7.344GiB
Name: apilab
ID: WER7:OD6C:ZPCQ:4QW6:R4AZ:67QA:FD7L:ODMI:7LDN:CEUY:GS6F:75BE
Docker Root Dir: /var/lib/docker
Debug Mode (client): false
Debug Mode (server): false
Registry: https://index.docker.io/v1/
Labels:
Experimental: false
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false

```

Questo comando fornisce informazioni sulla modalità in cui sta funzionando
Docker (modalità swarm o meno), quale driver di archiviazione viene
utilizzato per il *union filesystem*, quale versione del Kernel Linux
è disponibile sul nostro *host* e molto altro.

**Osservare attentamente l'output generato dal sistema durante l'esecuzione del comando.**

Procedere all'[esercizio 8](./exercise8.md).