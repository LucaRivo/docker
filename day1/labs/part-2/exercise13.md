# Reinterpretare il dichiarativo verso imperativo

Il Compose è uno strumento fornito da Docker che viene principalmente utilizzato
per eseguire e orchestrare i container in esecuzione su un singolo host Docker.

Ciò include ma non si limita allo sviluppo, all'integrazione continua (CI), ai
test automatizzati e al QA (*Quality Assurance*) manuale.

Il Compose utilizza i file formattati in YAML come input. Di default, 
*Docker Compose* prevede che questi file vengano chiamati `docker-compose.yml`,
ma sono possibili altri nomi. Si dice che il contenuto del file `mobile-compose.yml`
sia un modo dichiarativo di descrivere ed eseguire un'applicazione containerizzata
potenzialmente costituita da almeno un container.

Quindi:

- **Imperativo**: viene descritto come
      *Se dico al demone Docker imperativamente come eseguire un'applicazione,
       ciò significa che devo descrivere passo a passo cosa deve fare il sistema
       e come deve reagire se si verificano situazioni impreviste. Devo essere
       molto esplicito e preciso nelle mie istruzioni. Devo coprire tutti i casi
       limite e come devono essere trattati*;

- **Dichiarativo**: viene descritto come
      *Io dichiaro all'Engine Docker quale è il mio stato desiderato per un'applicazione
       e Docker deve capire, in autonomia, come raggiungere questo stato desiderato
       e come riconciliarlo se il sistema si discosta da esso*.

Docker raccomanda chiaramente l'approccio dichiarativo quando si tratta di
applicazioni containerizzate. Di conseguenza, lo strumento Docker Compose
utilizza questo approccio.

Procedere all'[esercizio 14](./exercise14.md).