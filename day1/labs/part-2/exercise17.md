# Quiz Game

Per valutare le tue abilità, prova a rispondere alle seguenti domande:

1. Come usare il docker-compose per eseguire un'applicazione in modalità daemon?

2. Come usare il docker-compose per visualizzare lo stato dei servizi in esecuzione?

3. Come scalare un particolare servizio web per dire, tre istanze?
