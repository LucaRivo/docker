# Eseguire un container in un namespace di rete esistente

Normalmente, Docker crea un nuovo namespace di rete per ogni container che
gestiamo. Il namespace di rete del container corrisponde al sandbox del
CNM descritto in precedenza.

Quando colleghiamo il container a una rete, definiamo un endpoint che collega
il namespace della rete del container con la rete attuale.

In questo modo, abbiamo un container per namespace di rete.

Docker fornisce un ulteriore modo per definire lo spazio dei nomi di rete in
cui viene eseguito un container. Quando creiamo un nuovo container, possiamo
specificare che il container deve essere collegato o forse dovremmo dire
incluso nel namespace di rete di un container esistente. Con questa tecnica,
possiamo eseguire più container nello stesso namespace di rete.

Ora, dimostriamo come funziona.

**Esercizio autonomo**:
Creare  una nuova rete di tipo `bridge` e di nome `test-net`.

**Esercizio autonomo/segue**:

Creare un container di nome `web`, deve eseguire in modalità `daemon`, collegato
alla rete `test-net` ed istanziare l'immagine Docker `nginx:alpine`.

Infine, eseguiamo un altro container e lo colleghiamo alla rete del nostro
container web:

```bash

docker container run -it --rm --network container:web alpine:latest /bin/sh

```

Nello specifico, notare come si definisce la rete: `--network container: web`.

La direttiva dice a Docker che il nuovo container utilizzerà lo stesso namespace
di rete del container chiamato web.

Poiché il nuovo container si trova nello stesso namespace di rete del
container `web`; ora siamo in grado di accedere a Nginx tramite `localhost`!

**Domanda**:

Qualcuno riesce a vedere qualche similitudine con il modello Pod dell'orchestratore
Kubernetes?

Possiamo fare il test utilizzando lo strumento `wget`, che fa parte del container
Alpine, per connetterci a Nginx. Dovremmo vedere quanto segue:

```bash

wget -qO - localhost

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
...
</html>

```

I container possono comunicare liberamente tra di loro, ma in quest'ultimo caso,
la comunicazione avviene tramite `localhost`.

Prima di procedere avanti con il prossimo esercizio, pulire il contesto:

```bash

docker container rm --force web
docker network rm test-net

```

Procedere all'[esercizio 6](./exercise6.md).