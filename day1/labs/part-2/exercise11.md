# Quiz Game

Per valutare le tue abilità, prova a rispondere alle seguenti domande:

1. Recuperare l'informazione del Kernel. Indicare e descrivere il comando
    da utilizzare;

2. Quali sono le variabili d'ambiente che permettono la configurazione
    del client Docker per la comunicazione con il server Docker remoto?;

3. Descrivere i diversi scenari per la pulizia del contesto di esecuzione
    del demone Docker. Accompagnare la descrizione anche i relativi comandi
    CLI dello strumento Docker.

Procedere all'[esercizio 12](./exercise12.md).