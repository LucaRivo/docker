# Il Container Networking Model (CNM)

Finora, abbiamo lavorato con singoli container. Ma in realtà,
un'applicazione aziendale containerizzata è composta da diversi
container che devono collaborare per raggiungere un obiettivo.
Pertanto, abbiamo bisogno di un modo in cui i singoli container
possano comunicare tra di loro. Ciò si ottiene creando delle
reti Docker che possiamo usare per inviare pacchetti di dati
avanti e indietro tra i container.

Docker ha definito un modello di rete molto semplice, il
cosiddetto modello di rete del container (CNM). Quanto segue
è una rappresentazione grafica del CNM:

![](./images/cnm.png)

Le componenti del modello di rete CNM sono:

1. **Sandbox**: il *Sandbox* isola perfettamente un container dal
    mondo esterno. Nessuna connessione di rete in entrata è consentita.
    Tuttavia, è molto improbabile che un container abbia un qualche
    valore aggiunto se è assolutamente impossibile comunicare con esso.
    Per ovviare a ciò, abbiamo l'elemento numero due, che è l'*Endpoint*;

2. **Endpoint**: un *endpoint* è come un *gateway*, che protegge il container.
    L'endpoint collega il sandbox (ma non il container) al terzo elemento
    del modello, che è la rete;

3. **Network**: La rete rappresenta un percorso che trasporta i pacchetti
    di dati di un'istanza di comunicazione, dall'endpoint sorgente
    all'endpoint destinazione. Infatti, dal container sorgente al
    container destinazione.

È importante notare che un sandbox di rete può avere zero o più endpoint
o, in altri termini, ogni container che vive in un sandbox di rete possa
essere collegato a nessuna rete o che possa essere collegato a più reti
contemporaneamente.

Certamente, il CNM è solo un modello che descrive il funzionamento
del networking tra i container.

Nella tabella a seguire, proponiamo degli esempi di rete comunemente utilizzate:

|Network|Produttore|Scope|Descrizione|
|:------|:---------|:----|:----------|
|Bridge|Docker|Local|Semplice tipologia di rete, basata sul concetto di Bridge Linux. questa tipologia di rete permette la comunicazione inter container in modalità a host singolo di Docker.|
|Overlay|Docker|Global| Un tipologia di rete che abilita la comunicazione tra container in modalità multi host di Docker.|

Procedere all'[esercizio 1](./exercise1.md).