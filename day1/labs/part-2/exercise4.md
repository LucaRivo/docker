# La rete Null

A volte, è necessario eseguire alcuni servizi applicativi senza alcuna
connessione di rete. Si consiglia vivamente di eseguire tali applicazioni
in un container collegato alla rete `none`. Questo container sarà completamente
isolato e quindi al sicuro da qualsiasi accesso esterno. Facciamo un tale container:

```bash

docker container run --rm -it --network none alpine:latest /bin/sh

```

**Esercizio autonomo**:

Una volta all'interno del container, verificare che non è disponibile alcun
endpoint di rete `eth0`.

**Esercizio autonomo**:

Verificare, una volta all'interno del container che nemmeno esiste alcuna
regola di routing.

Procedere all'[esercizio 5](./exercise5.md).