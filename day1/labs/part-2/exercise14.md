# Eseguire un’applicazione complessa

Nella maggior parte dei casi, le applicazioni non consistono di in un solo blocco
monolitico, ma piuttosto in diversi servizi applicativi che funzionano insieme.
Quando si utilizzano i container Docker, ogni servizio applicativo viene eseguito
nel proprio container.

Quando vogliamo eseguire un'applicazione multi-service, possiamo ovviamente
avviare tutti i container partecipanti con il ben noto comando di esecuzione del
container. *Qual è questo comando?*

Ma questo è inefficiente nella maggioranza dei casi. *Perché?*

Con lo strumento Docker Compose, viene fornito un modo per definire l'applicazione
in modo dichiarativo in un file che utilizza il formato YAML.

Diamo un'occhiata al contenuto di un semplice file `docker-compose.yml`:

```YAML

version: "3.5"
services:
  web:
    image: repo.treescale.com/andov/tutorialia-web:1.0
    ports:
      - 3000:3000
  db:
    image: repo.treescale.com/andov/tutorialia-database:1.0
    volumes:
      - pets-data:/var/lib/postgresql/data

volumes:
  pets-data:


```

Il contenuto delle righe nel file *Compose* sono spiegate come segue:

- *version*: ogni Compose file ha una versione associata con una precisa versione
    del demone Docker; non tutte le versioni vanno bene tra di loro. La versione
    più recente è retro compatibile con le versioni meno recenti;

- *services*: sezione per la definizione delle componenti applicative costituenti
    l'architettura complessiva:

  - `web`: il servizio utilizza l'immagine `repo.treescale.com/andov/tutorialia-web`
    e pubblica la porta `3000` sul host Docker;

  - `db`: il servizio utilizza l'immagine `repo.treescale.com/andov/tutorialia-database`
    e non espone alcuna porta. Inoltre, il servizio `db` utilizza un volume dati
    chiamato `pets-data`;

- *volumes*: i volumi utilizzati da uno qualsiasi dei servizi devono essere
    dichiarati in questa sezione. Nel nostro esempio, questa è l'ultima sezione
    del file. La prima volta che viene eseguita l'applicazione, un volume chiamato
    `pets-data` verrà creato da Docker e quindi, nelle esecuzioni successive, se
    il volume è ancora presente, verrà riutilizzato. Questo potrebbe essere
    importante quando l'applicazione, per qualche motivo, si blocca e deve essere
    riavviata. Quindi, i dati precedenti sono ancora disponibili e pronti per essere
    utilizzati dal servizio di database riavviato.

Navigare internamente alla directory `demo-docker-compose` che è presente seguendo
il seguente path `day1/labs/part-2/demo-docker-compose` ed eseguire:

```bash

# Per la modalità bloccante
docker-compose up

# Per la modalità non bloccante
docker-compose up -d

```

Lo strumento assumerà che ci sia un file nella directory corrente chiamato
`docker-compose.yml` e userà questo per l'esecuzione.

**Esercizio autonomo**:
Come eseguire un file Docker Compose con un nome diverso da `docker-compose`?

Ora possiamo aprire una scheda del browser e navigare a `http://${AWS_OWN_URL}:3000/pet`.
Con `${AWS_OWN_URL}` il nome DNS della propria macchina di laboratorio.

Aggiorna il browser alcune volte.

Per fermare e ripulire l'ambiente, usiamo il comando `docker-compose down` per la versione
non bloccante e `CTRL+C` per la versione bloccante.

Procedere all'[esercizio 15](./exercise15.md).