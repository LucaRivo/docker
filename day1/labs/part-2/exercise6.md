# Quiz Game/Networking

Per valutare le tue abilità, prova a rispondere alle seguenti domande:

1. Fornire i nomi dei tre elementi principali del modello CNM.

2. Come creare una rete `bridge` personalizzata chiamata, ad esempio, `frontend`?

3. Come gestire due istanze nginx:alpine collegati entrambi alla stesa rete `frontend`?

4. Qual è lo scopo della rete *host*?

5. Indicare uno o due scenari in cui l'utilizzo della rete *host* è appropriato.

6. Qual è lo scopo della rete *none*?

7. In quali scenari dovrebbe essere utilizzata la rete *none*?

Procedere all'[esercizio 7](./exercise7.md).