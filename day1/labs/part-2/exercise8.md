# Ottenere informazioni sul consumo di risorse

Nel tempo, un host Docker può accumulare un bel po' di risorse come immagini,
container e volumi e su disco. Come buona prassi, dovremmo mantenere pulito il
nostro ambiente e liberare risorse inutilizzate. Altrimenti, arriverà il momento
in cui Docker non ci permetterà di aggiungere altre nuove risorse, il che
significa che azioni come il pull di un'immagine possono fallire a causa della
mancanza di spazio disponibile su disco o in memoria.

La CLI di Docker fornisce un pratico piccolo comando di sistema che elenca la
quantità di risorse attualmente utilizzate nel nostro sistema e la quantità di
spazio che è possibile recuperare. Il comando è:

```bash

docker system df

```

Se esegui questo comando sul tuo sistema, dovresti vedere un risultato
simile a questo:

```bash

TYPE                TOTAL               ACTIVE              SIZE                RECLAIMABLE
Images              1                   1                   4.148MB             0B (0%)
Containers          3                   0                   151B                151B (100%)
Local Volumes       0                   0                   0B                  0B
Build Cache                                                 0B                  0B

```

L'ultima riga nell'output, *Build Cache*, viene visualizzata solo nelle versioni
più recenti di Docker. Questa informazione è stata aggiunta di recente. L'output
precedente è spiegato come segue:

- Nel mio caso, il numero di immagini Docker presenti sul mio sistema
    operativo e quante immagini sono in stato di *running*;

- Inoltre, il numero di container che sono presenti in `/var/lib/docker/containers`
    e quanto spazio occupano;

- Il numero di volumi creati e quanto spazio occupano; nel mio caso
    0B;

- Infine, il commando ci offre un'indicazione sulla grandezza della
    cache locale utilizzata per il *build* delle immagini Docker.

Se vogliamo informazioni ancora più dettagliate sul consumo di risorse
sul nostro sistema, possiamo eseguire lo stesso comando in modalità
dettagliata usando il flag -v:

```bash

docker system df -v

```

Questo ci fornirà un elenco dettagliato di tutte le immagini, i container
e i volumi con le rispettive dimensioni. Un possibile risultato potrebbe
essere simile a questo:

```bash

Images space usage:

REPOSITORY          TAG                 IMAGE ID            CREATED ago         SIZE                SHARED SIZE         UNIQUE SiZE         CONTAINERS
alpine              latest              3fd9065eaf02        4 months ago ago    4.148MB             0B                  4.148MB             3

Containers space usage:

CONTAINER ID        IMAGE               COMMAND             LOCAL VOLUMES       SIZE                CREATED ago         STATUS                    NAMES
415cdb52371e        alpine:latest       "ping 127.0.0.1"    0                   10B                 2 days ago ago      Exited (137) 2 days ago   c4
f44238e7565d        alpine:latest       "ping 127.0.0.1"    0                   141B                2 days ago ago      Exited (137) 2 days ago   c3
66fb3de1f0ae        alpine:latest       "ping 127.0.0.1"    0                   0B                  2 days ago ago      Exited (137) 2 days ago   c2

Local Volumes space usage:

VOLUME NAME         LINKS               SIZE

Build cache usage: 0B


```

Questo output verboso dovrebbe fornirci informazioni dettagliate sufficienti per
prendere una decisione informata sull'opportunità o meno di iniziare a ripulire
il nostro sistema e su quali parti potremmo aver bisogno di ripulire.

Procedere all'[esercizio 9](./exercise9.md).