# Software defined Firewall

Il modello di rete offerto da Docker è semplice.
Esso permette di definire in modo
semplice ed economico una rete. Oltre alla
definizione di una rete, il modello CNM
implementa un *software defined firewall* tra
container che appartengono a reti diverse.

Vediamo questo ultimo concetto tramite un esempio:

![](./images/firewall-cnm.png)

Nella precedente immagine, abbiamo due reti,
Network:front (arancione) e Network:back (verde).
Alla rete Network:front sono connessi i container
c{0,1}. Mentre alla rete Network:back sono connessi
i container c{3,4}.

All'interno delle precedenti reti, i container c1, c2 e
c3, c4 sono in grado di comunicare liberamente.
Tuttavia, i container connessi alla rete Network:back
non possono comunicare con i container connessi alla rete
Network:front.

Passando da una vista astratta ad un esempio più concreto,
supponiamo di avere un'applicazione consistente di tre servizi:
**webAPI**, **productCatalog**, **database**.

I nostri requisiti di comunicazione sono i seguenti:

1. Il servizio di *webAPI* deve essere in grado di comunicazione con il servizio *productCatalog*;

2. Il servizio di *webAPI* **non-deve** essere in grado di comunicare con il servizio *database*;

3. Il servizio *productCatalog* deve essere in grado di contattare il servizio *database*.

A questo punto, per implementare i requisiti di comunicazione si
propone la progettazione e l'implementazione di reti distinte per
segregare il traffico tra le componenti.

Proponiamo una possibile soluzione nell'immagine che segue:

![](./images/example-cnm.png)

Docker caldamente incentiva l'approccio presentato sopra:
definire gli attori che necessitano di comunicazione ed
implementare molteplici reti per isolare la comunicazione tra di loro.

Procedere all'[esercizio 2](./exercise2.md).