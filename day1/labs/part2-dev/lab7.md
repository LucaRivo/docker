# Scheduling e Running

Nel docker-compose si possono inserire una serie di direttive che riguardano la parte di scheduling e running dei task.

Si tratta di:

* Replicas ( scalabilità )
* Update Config ( tra le modalità di deployment )
* Placement
* Healthcheck
* Restart policy

## Replicas ( Scalabilità )

Tramite Swarm si possono aumentare / diminuire il numero di repliche tramite il comando:

```
docker service scale <NOME SERVIZIO>=<NUMERO REPLICHE>
```

La stessa cosa può farsi a livello di docker compose:

```
 backend:
    image: samplebe-ws:1.0
    ports:
      - "8085"
    deploy:
      replicas: 2
```

Esercizio: aumentare il numero delle repliche a 6 per i task di backend. Cosa succede se si effettua una operazione di kill su uno dei container attivi ?


Questo approccio permette di gestire situazioni nella fase di runtime. E’ opportuno che, nel caso la modifica del numero delle repliche diventi permanente, occorre riportarla a livello di docker-compose. In modo che venga versionata correttamente nel repository delle configurazioni.

## Update Config ( Modalità di Deployment )

Tramite Docker si possono implementate varie politiche di deployment di uno stack. 

Nel caso più semplice si può eliminare lo stack e crearne uno di nuovo.
Oppure sfruttando la possibilità di mantenere contemporaneamente attivi stack in parallelo si può implementare un modello blue-green deployment.

Una funzionalità messa a disposizione dalla piattaforma prevede di effettuare un rolling update: 

```
backend:
    image: samplebe-ws:2.0
    ports:
      - "8085"
    deploy:
      replicas: 6
      update_config:
        delay: 15s
        parallelism: 1
        failure_action: rollback
    networks:
      - net
```
      
In questa configurazione viene eseguito un update di un task alla volta ( parallelism=1 ) ogni 15 secondi.

Esercizio: aggiornare il proprio stack, facendo l’upgrade della versione dell’immagine ( passando alla versione samplebe-ws:2.0 )

## Placement

Con le direttive di placement, vengono fornite a Swam indicazioni riguardo al nodo su cui eseguire un task. Questo si rivela utile nei casi in cui si voglia che un determinato container necessiti di un particolare posizionamento ( es: per motivi di licenze ,vincoli tecnologici o altro ).

Il placement esegue un match tra le label che caratterizzano un nodo ed i vincoli indicati a livello di compose.

L’attribuzione/rimozione di una etichetta al nodo si esegue con i comandi:

```
docker node update --label-add CHIAVE=VALORE NODE
docker node update --label-rm  CHIAVE NODE
```

Domanda: come si possono controllare le labels definite su un determinato nodo ?

La definizione a livello di docker-compose si configura con:

```
deploy:
      placement:
        constraints:
          - node.labels.CHIAVE == VALORE
```

Esercizio:

Attribuire al nodo della propria installazione Swarm l'etichetta **appdynamics_enabled** valorizzata a **true**

Questo serve per fare in modo che Swarm posizionerà il task in un nodo etichettato con il valore appdynamics_enabled==true.

Scrivere quindi un Docker compose che utilizzi la direttiva di placement in modo che il task venga posizionato in un nodo compatibile con Appdynamics.
Modificare il Docker compose settando appdynamics_enabled==false e capire il comportamento di Swarm in questo caso.

( nota: il tool di monitoraggio Appdynamics è un caso reale )

## Healthcheck

La funzionalità di healthcheck permette a Swarm di conoscere lo stato dei task e di reagire di conseguenza. 

Di default viene controllato lo stato del container, ma per un controllo più accurato è necessario specificare un comando di healthcheck. Ad esempio un container potrebbe essere attivo, ma l’applicazione non rispondere correttamente, magari per un problema di Out Of Memory.

E’ possibile quindi specificare un comando, periodicamente invocato da Swarm, definito nel docker-compose:

```
     healthcheck:
        test: ["CMD", "wget", "-qO", "-", "http://localhost"]
        interval: 2s
        timeout: 2s
        retries: 3
        start_period: 2s
```

Per quanto riguarda la sintassi, la sezione healthchek deve essere inserita allo stesso livello di "deploy".

Parametro | Note
------------ | ------------   
interval | il tempo di attesa tra due controlli consecutivi di health
timeout | specifica il tempo che Docker deve attendere prima di considerare la richiesta in timeout
retries | il numero di failure consecutivi consentiti
start_period | il tempo di bootstrap prima di iniziare i controlli


Per quanto riguarda l'exit status del comando di healthcheck:

* 0: success - container healthy
* 1: unhealthy - in container non lavora correttamente
* 2: reserved - da non utilizzare
 

## Restart Policy

Forniscono indicazioni sul come restartare i container quando escono in modo non corretto.

```
 deploy:
   restart_policy: 
     condition: on-failure 
     delay: 5s 
     max_attempts: 3 
     window: 120s
```

Si configurano nella sezione deploy.

Opzione | Valori | Note
------------ | ------------ | ------------  
condition | none, on-failure, any (default: any) | on-failure quando il task esce con un exit status diverso da zero
delay | (default: 0) | tempo di attesa tra i tentativi di restart
max_attempts |  | quante volte si tenta il restart
window |  | quanto tempo attendere per capire se un restart ha avuto successo o meno


















