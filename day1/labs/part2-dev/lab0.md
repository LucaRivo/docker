# Docker Swarm


## Introduzione

Docker può essere utilizzato in modalità *Single Docker Engine* oppure con l’orchestratore *Docker Swarm*.

![Docker Swarm ](./images/Docker-swarm.png)

Nel primo caso si utilizza un unico host, dove comunque si possono far girare molteplici container. 
I comandi di gestione agiscono su un singolo container.

In alternativa, sfruttando la componente di orchestrazione Swarm, si possono gestire in modo centralizzato molteplici host. 
In questa modalità si opera a livello di **stack**, che comprende più container che cooperano per l’implementazione di un servizio applicativo. 
Dal punto di vista infrastrutturale, Swarm si compone di una serie di nodi. 
Alcuni sono riservati alla gestione della piattaforma ( Manager ), altri utilizzabili per l’esecuzione dei container ( worker ). 
In un’installazione su singolo nodo il manager ed il worker coincidono.

L'orchestratore è già presente nativamente nell'installazione di Docker per cui non sono necessarie componenti aggiuntive.


## Architettura Swarm

Swarm comprende una componente Manager che si occupa della gestione del cluster ed una Worker, utilizzata per l’esecuzione dei container.

![Docker Swarm Architecture](./images/Docker-swarm-architecture.png)

#### Swarm Manager:

Le principali componenti sono:

 1. Scheduler che si occupa di distribuire i container tra i vari nodi. Nel caso di fail di un nodo si preoccupa di ri-schedulare il workload su un nodo disponibile. 
 2. La parte di Discovery Service gestisce invece i nodi ( aggiunta, rimozione ) in modo automatico. La gestione della capacity si ottiene aggiungendo nodi worker al cluster Swarm.

#### Swarm Worker Node:

Comprende i sistemi utilizzati per l’esecuzione dei container.

## Docker Compose - Stack - Service - Task

Le applicazioni vengono descritte nel file docker-compose, per la realizzazione di quello che viene definito come stack.
Nello stack vengono definiti una serie di servizi ( es: frontend, backend ) specificando le relative immagini da utilizzare.
Inoltre si definiscono una serie elementi ( network, secrets, configs ) che concorrono alla definizione del servizio.
Questi elementi, verranno creati nella fase di "creazione" dello stack ( operazione di deploy).

L'approccio è dichiarativo, ovvero si definiscono i vari parametri di configurazione che la piattaforma si preoccupa poi di applicare.

Il nodo su cui verrà istanziato il singolo container è deciso dall'orchestratore Swarm. 
Si possono/devono comunque indicare delle label di placement che verranno utilizzate per selezionare i nodi su cui mandare in esecuzione i container.

ESEMPIO: alcuni nodi possono essere etichettati con una label "location" che specifica se l'installazione è on-premise oppure cloud.
Nel compose si dovrà specificare questa label , in modo che i container verranno attestati sui nodi corretti.

Le immagini dichiarate all’interno del docker-compose, devono esistere nel registry per poter essere utilizzate.


![Docker Swarm Stack](./images/Docker-swarm-stack.png)

Lo stack quindi rappresenta l’elemento che astrae un’applicazione. 

Il servizio è legato alla parte implementativa all’interno dello stack. A questo livello viene specificata l’immagine da utilizzare per lo specifico servizio. Insieme al resto delle informazioni necessarie per la definizione e la gestione dello stesso. Come ad esempio le porte da utilizzare, le reti, il numero di repliche…

Il task rappresenta il container, nell’architettura Swarm. Ogni task è gestito dallo scheduler, che lo alloca su un nodo worker, in base alla disponibilità di risorse ed ai vincoli di placement che indichiamo.

[Comandi base Swarm](./lab1.md)
