# Gestione Secrets

Come indicato nella parte introduttiva della gestione delle configurazioni, in presenza di dati confidenziali, la funzionalità da utilizzarsi è quella dei secrets.

Il funzionamento è simile a quella dei configs.

## Comandi di gestione

Per la creazione di un secret, a livello di piattaforma Swarm:

```
docker secret create <NOME SECRET> <FILE SECRET>
```

Lista secret :

```
docker secret ls
```

Rimozione :

```
docker secret rm <NOME SECRET>
```

Per ispezionare uno config :

```
docker secret inspect <NOME SECRET>
```

## Esempio di utilizzo in un docker-compose

```
version: '3.3'

networks:
  net:
    driver: overlay

secrets:
  my_secret:
    external: true

services:
  frontend:
    image: samplefe:1.0
    ports:
      - "8080:8080"
    secrets:
      - my_secret
    networks:
      - net
```

L’utilizzo di un secret avviene tramite la definizione  all’interno del file docker-compose.yml. Il secret verrà «montato» all’interno del container nella cartella /run/secrets.

In questo esempio viene indicato un secret chiamato «my_secret». External indica che il secret viene definito separatamente a livello di piattaforma Swarm

All’interno della definizione del servizio,  il secret viene referenziato, indicandone il nome.

## Note su Secrets e Configs

Le funzionalità di config e secrets sono simili: per entrambi si definiscono degli elementi di configurazione che vengono iniettati all’interno dei container. 

Per i secrets però la destinazione è **/run/secrets** mentre per i config si può scegliere la cartella di «mount».

I secrets vengono cifrati all’interno della piattaforma per mantenere la confidenzialità. Un config invece è ricavabile tramite una operazione di inspect.

Tra le attività legate allo sviluppo occorre prevedere la gestione di questi files, in modo coerente con quanto si aspetta l’applicazione. 

Questi meccanismi sono disponbili sono su piattaforma Swarm. Nel caso invece si utilizzi Docker nella versione single engine, è comunque possibile simulare questa funzionalità, eseguendo un bind mount dei files all’interno dei container.



[Scheduling e Running](./lab7.md)