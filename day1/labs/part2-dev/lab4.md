# Gestione configurazione in ambito Container

## Introduzione

Una best practice indicata da Docker, prevede di mantenere quanto più contenuto il numero delle immagini create, per evitare proliferazioni e la conseguente difficoltà di gestione. Questo non riguarda tanto la parte di versionamento dell’applicazione, quanto piuttosto la modalità di creazione dei diversi ambienti necessari per il ciclo di sviluppo del software.

Non devono essere create specifiche immagini per ambiente, ma è preferibile un'unica immagine riutilizzabile, da usarsi in abbinata con una parte di configurazione variabile.

Questo approccio comporta una rivisitazione della gestione delle configurazioni. Si devono adottare meccanismi di configurazione dinamica da utilizzare all'interno dei container. Rispetto a quanto si effettua in una infrastruttura tradizionale, con una configurazione custom, definita ambiente per ambiente.

Per la gestione delle configurazioni, l'infrastruttura Docker mette a disposizione due funzionalità:
* secret: da utilizzarsi in presenza di dati sensibili ( es: password )
* config: per gli altri file di configurazione

Il principio di funzionamento è lo stesso: dopo avere configurato questi elementi secondo le modalità previste da Docker, i file di configurazione verranno montati all'interno del container al momento dell'avvio.
Da tenere presente che è presente un vincolo sulla dimensione del contenuto, che non deve superare i 500Kb.

## Gestione configs

La creazione di un config, a livello di piattaforma Swarm, :

```
docker config create <NOME CONFIG> <FILE DI CONFIGURAZIONE>
```

Lista config :

```
docker config ls
```

Rimozione :

```
docker config rm <NOME CONFIG>
```

Per ispezionare un config :

```
docker config inspect <NOME CONFIG>
```

## Utilizzo di un config da parte di un task

Modifichiamo il docker-compose.yml in modo da introdurre la gestione dei config

```
version: '3.3'

networks:
  net:
    driver: overlay

configs:
  demosvil:
    external: true

services:
  frontend:
    image: samplefe:1.0
    ports:
      - "8080:8080"
    configs:
      - source: demosvil
        target: /opt/app/conf/application.properties
    networks:
      - net
```      

In questo esempio viene indicato un config chiamato «demosvil». External fa riferimento ad un config definito a livello di piattaforma Swarm.

All’interno della definizione del servizio, viene referenziato il config, indicandone il nome ed il posto dove verrà iniettato all’interno del container.

## Hands On

Creare un config, inserirlo all'interno di uno stack e verificare che il file di configurazione viene effettivamente inserito all'interno del container.

[Compose multi servizio](./lab5.md)
