# Overview Conclusiva

## Funzionalità Swarm

![Docker Swarm Highlights](./images/Docker-swarm-highlights.png)


Cluster management integrated with Docker Engine: Swarm è un tool nativo Docker. Una volta inizializzato il cluster, si possono deployare gli stack applicativi senza bisogno di ulteriori strumenti

Secrets / Configs: queste funzionalità permettono di gestire le configurazioni in modo separato dalle immagini. E di iniettare dentro ad un container, un set di files di configurazione. Un «ambiente» diventa quindi l’abbinata di una serie di immagini con le relative configurazioni. Il tutto dichiarato all’interno dello stack. Nei secrets ( cifrati ) vengono gestiti i dati sensibili.

Declarative service model: il compose è un file dichiarativo. Alla piattaforma viene delegato il compito di mantenere lo stato consistente con la dichiarazione. Ad esempio, se nel compose è dichiarato che devono essere presenti due istanze di backend, ed una dovesse andare in crash, Swarm provvederà autonomamente ad istanziare un nuovo container.

Multi-host networking: Swarm attribuisce automaticamente un indirizzo IP ad ogni container, attestandolo nella network indicata nel compose. Questo permette di segregare i vari servizi. 

Service discovery: ogni servizio che viene definito nel compose viene mappato in un DNS interno. Il nome del servizio può quindi essere utilizzato come hostname. Ad esempio un frontend che deve richiamare un servizio chiamato polaris_backend dovrà utilizzare questo hostname a livello di configurazione. Rispetto ad una infrastruttura tradizionale, non occorre quindi il coinvolgimento di altre strutture per definire configurazioni DNS.

Load balancing / Scaling: una volta definito un servizio ed indicato un numero di container , la piattaforma provvede automaticamente a bilanciare le richieste verso i container interessati. Tramite comandi CLI è possibile aumentare o diminuire il numero di container che erogano un determinato servizio. Il load balancing interno provvederà ad indirizzare le richieste anche verso i nuovi container.

Rolling updates: la piattaforma dispone di meccanismi per effettuare l’aggiornamento di uno stack, riavviando i servizi interessati



## Vantaggi derivanti dall'utilizzo

Le funzionalità messe a disposizione da una piattaforma Docker, permettono di semplificare le attività sia della parte DEV che di quella OPS.
Riassumendo i principali vantaggi:

* Tempi ridotti nel provisioning degli ambienti
* Abilitazione all’utilizzo di diverse tecnologie senza problemi di compatibilità
* Ripetibilità del contesto di esecuzione
* Semplificazione nella condivisione di servizi applicativi
* Versionamento, non solo del codice applicativo ma anche dell’infrastruttura (tramite Dockerfile – docker-compose)
* Abilitazione ai test automatici
* Autoconsistenza a livello di container ( librerie, MW, tools, ) che risolve problemi di missing dependencies
* Contesto isolato ( si possono provare in parallelo più versioni della stessa applicazione )
* Ablitazione alla scalabilità dei servizi
