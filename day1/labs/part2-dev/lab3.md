# Esempio Docker Compose

```
version: '3.3'

networks:
  net:
    driver: overlay

services:
  frontend:
    image: samplefe:1.0
    ports:
      - "8080:8080"
    environment:
      - loglevel=warn
    networks:
      - net
```

Questo è un esempio minimale di docker-compose. Si tratta di un file testuale in formato YAML.
Per cui occorre rispettare le regole della sintassi di questo formato:

* Vengono utilizzati gli spazi ( e non le tabulazioni ) per denotare le strutture;
* Il commenti iniziano con il simbolo del cancelletto (#);
* Le liste sono denotate con il simbolo del trattino (–);

Un compose deve iniziare con la specifica della versione, per assicurare la compatibilità con la piattaforma target di riferimento.

La sezione successiva rappresenta l’insieme delle risorse comuni ( network, configurazioni,… )

Nell’ultima sezione vengono inseriti i servizi. In questo caso è presente un unico servizio ( frontend ) caratterizzato dalla sua immagine, dalle porte utilizzate e dalla parte di rete. 

## Comandi gestione stack

Per elencare gli stack presenti:

```
docker stack ls
```

Per creare uno stack o effettuare una modifica ad uno stack esistente:
```
docker stack deploy -c <COMPOSE FILE> <STACK NAME>
```
Rimozione :
```
docker stack rm <STACK NAME>
```
Per ispezionare uno stack :
```
docker stack inspect <STACK NAME>
```

Per elencare i servizi presenti in uno stack  ( da notare che il nome del servizio viene prefissato dal nome dello stack ):
```
docker service ls
```

## Hands On


Effettuare il deploy di uno stack, utilizzando il compose visto in precedenza, denominato «sviluppo1» e testare i comandi relativi alla gestione degli stack.

Personalizzare tramite la variabile di ambiente «sfondo» il colore di background della index.jsp.

Effettuare in parallelo un secondo deploy, con il nome «sviluppo2» con un diverso colore di sfondo. 

Se si volesse testare l’applicazione con una diversa versione dell’application server, come si deve procedere ?

[Gestione della Configurazione](./lab4.md)


