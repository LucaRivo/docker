# Resources - Network

Altri elementi che rientrano tra le configurazioni che si possono inserire all'interno del docker-compose riguarda l'utilizzo delle risorse e la parte network.

## Resources

Nella sezione deploy si possono settare dei limiti relativi all’utilizzo delle risorse.

Si tratta non solo di una best practice, ma di un vincolo per il deploy nella piattaforma centralizzata. La mancanza di limiti di una applicazione potrebbe causare disservizi ad altre applicazioni che girano nello stesso nodo.

La parte di limits indica il massimo di risorsa che un container può sfruttare.

La parte di reservations indica la quantità tipicamente utilizzata da un container.

Il comportamento di Swarm è diverso nel caso di RAM e CPU:
* se un container supera il limite di RAM viene terminato
* se supera il limite CPU viene rallentato ( throttled )

Esempio di configurazione:

```
deploy:
  resources:
    limits:
      cpus: '0.1'
      memory: 50M
    reservations:
      cpus: ‘0.01'
      memory: 20M
```

## Network

Tra gli elementi che si devono configurare nel docker-compose, oltre agli eventuali config/secrets occorre definire la parte network.

Senza questa definizione i container non sono in grado di comunicare tra loro.

Utilizzando più reti inoltre, possiamo segmentare opportunamente il sistema, pemettendo la comunicazione solo tra alcuni componenti escludendo gli altri.

![Docker Swarm Highlights](./images/Docker-swarm-network.png)

A livello di docker-compose si traduce nel definire due networks ed agganciarle ai servizi interessati:


```
version: '3.3'

networks:
  frontend:
    driver: overlay
  backend:
    driver: overlay


services:

  webserver:
    image: apache:2.2
    ports:
      - "80:80"
    networks:
      - frontend


  frontend:
    image: samplefe:1.0
    ports:
      - "8080:8080"
    deploy:
      replicas: 1
    environment:
      - loglevel=warn
    networks:
      - frontend
      - backend


  backend:
    image: samplebe-ws:1.0
    ports:
      - "8085"
    deploy:
      replicas: 1
    networks:
      - backend
```


## Routing Mesh

In questo schema viene mostrato come Swarm gestice le richieste provenienti dall'esterno.

![Docker Swarm Routing Mesh](./images/Docker-swarm-routingmesh.png)

Supponiamo di avere tre nodi ( Host A, Host B e Host C ) su cui è stato deployato uno stack con un servizio composto da due task ( ad esempio allocati su B e C ).

La creazione di un servizio comporta l’assegnazione allo stesso di un virtual IP (VIP), che resta associato al
corrispondente servizio per tutto il ciclo di vita. Assumiamo che l'indirizzo IP
sia 10.2.0.1.

Per l’utilizzo dall’esterno viene aperta la stessa porta ( definita a livello di compose ), su cui vengono indirizzate le chiamate dal load balancer esterno.




