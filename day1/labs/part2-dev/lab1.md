# Comandi base: inizializzazione e gestione nodi

## Swarm Init

Swarm è già presente all’interno di una installazione Docker. Non occorre quindi una installazione aggiuntiva, 
ma semplicemente basta inizializzare il cluster con il comando:

```
docker swarm init
```

L’output di questo comando è simile a :

```
Swarm initialized: current node (z9p3nga00yjugee5rwpyxq7vv) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-10kiyh2n5vzu54zqfnlmlzhhxgdqh0vbpet4gskczb9rx9nwac-c8n9w6kf00kecgq 172.25.22.7:2377
```

che indica come il nodo in questione sia diventato un manager ed indica il comando da utilizzare ( join ) per aggiungere nodi worker al cluster. 
L’analogo comando per togliere nodi utilizza l’opzione leave, permettendo di gestire in modo flessibile la nostra infrastruttura.

## Gestione Nodi

Per elencare tutti i nodi di un cluster:

```
docker node ls
```

Per ispezionare un nodo :

```
docker node inspect <NODE NAME>
```

Per controllare i task in esecuzione su di un nodo :

```
docker node ps <NODE NAME>
```

Per etichettare un nodo :

```
docker node update --label-add <CHIAVE>=<VALORE> <NODE NAME>
docker node update --label-rm <CHIAVE> <NODE NAME>
```



[Esempio Docker Compose](./lab3.md)