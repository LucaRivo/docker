# Stack multiservizio

# Esempio compose con più servizi

```
version: '3.3'

networks:
  net:
    driver: overlay

configs:
  demosvil:
    external: true

services:

  frontend:
    image: samplefe:1.0
    ports:
      - "8080:8080"
    configs:
      - source: demosvil
        target: /opt/app/conf/application.properties
    networks:
      - net


  backend:
    image: samplebe-ws:1.0
    ports:
      - "8085"
    networks:
      - net
```

Nella sezione «services» si possono inserire N servizi. In questo caso sono presenti un servizio frontend ed un servizio backend.

Questi non sono solo nomi del servizio ma sono proprio DNS name validi all’interno della piattaforma. 

## Hands On

1) Creare un config chiamato configurazione1 , utilizzando un file con questo contenuto, valorizzando la variabile host in modo che punti al backend.

```
titolo=Benvenuto nella Demo Application SVILUPPO
host=XXXX
port=8085
```

Testare i comandi relativi alle configurazioni visti in precedenza per il listing, l’ispezione, la cancellazione.

2) Tenendo conto che il file sopra indicato, è il file di configurazione dell’applicazione di frontend , realizzare uno stack completo, valorizzando opportunamente il puntamento verso il backend.

3) Creare una nuova configurazione ( configurazione2), con un diverso valore del titolo ed applicarla allo stack. 

4) Nel caso si desideri gestire la configurazione tramite files in formato .tar.gz da passare sempre tramite config, come si deve modificare l’approccio fin qui utilizzato ?

[Gestione dei Secrets](./lab6.md)