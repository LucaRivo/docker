# Istruzioni

## Introduzione

La sintassi delle istruzioni ( che per convenzione vengono scritte in maiuscolo ) è molto semplice:

```bash

# Commento
ISTRUCTION arguments

```

## FROM

```bash

FROM <image>[:tag] [AS <name>]

```

Docker prevede la creazione di nuove immagini tramite un meccanismo di estensione. 
L’immagine di base viene specificata dall’istruzione FROM. Ogni nuovo layer viene aggiunto “sopra” l’immagine base.

FROM è tipicamente la prima istruzione che compone il Dockerfile. 
Può essere preceduta dall'istruzione ARG, che indica una variabile che può essere utilizzata all'interno della build.

```bash

ARG  TOMCAT_VERSION=8
FROM tomcat:${TOMCAT_VERSION}

```

## ARG

```bash

ARG <name>[=<default value>]

```

ARG definisce una variabile da utilizzare a build-time. 
Può essere passata come argomento, al comando docker build, con il flag ``` --build-arg <varname>=<value> ```.

Lo scope della variabile è limitato alla fase di build.

## LABEL

```bash

LABEL <key>=<value> <key>=<value> <key>=<value> ...

```

L'istruzione LABEL aggiunge metadati all'immagine. 
Utilizza il formato chiave=valore . Di seguito alcuni esempi:

```bash

LABEL "com.example.vendor"="ACME Incorporated"
LABEL com.example.label-with-value="foo"
LABEL license="GPLv2"
LABEL description="This text illustrates \
that label-values can span multiple lines."

```

Le label possono anche essere aggiunte dinamicamente nel comando di build:

```bash

docker build . --label "version=1.0" ....

```


## ADD/COPY

```bash

COPY <local/file/path> </image/file/path>
ADD <http://domain.com/file> </image/file/path>
ADD <file.tar.gz> </image/path/>

```

Entrambe le istruzioni servono per inserire files all’interno dell’immagine, ma operano in modo differente.

-	COPY copia files o cartelle dall’host, non opera con contenuti remoti
-	ADD oltre alla copia esegue anche l’autoestrazione per le estensioni note ( es tar ). Inoltre può essere utilizzata con contenuti remoti

Le best practices indicano di preferire la COPY rispetto alla ADD, per un maggior controllo sul processo di build. 
L'utilizzo migliore dipende però dalla specifica situazione che si deve affrontare

## Hands On

Esempio 1: si desidera creare un container custom, con un Tomcat in versione 8 e l'applicazione travel.war. 
E' già previsto che l'applicazioni migri alla versione Tomcat 9, nel breve periodo.
Nell'immagine ufficiale di tomcat, la cartella di deploy è la /usr/local/tomcat/webapps/ . 
Utilizzare dei metadati, tramite delle apposite LABEL, in modo che siano recuperabili tramite l'operazione di inspect.

Esempio 2: modificare alcune LABEL e ricreare l'immagine per verificare gli impatti dell'utilizzo della cache, sul processo di build.

Esempio 3: ricreare l'immagine , utilizzando la versione di Tomcat 9




[Istruzioni, parte seconda](./exercise1.2.md).