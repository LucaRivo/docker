# Build multi-stage

La funzionalità di multi-stage build è disponibile dalla versione di Docker 17.05 e successive, sia per il client che per il demone server. 
E' una funzionalità che serve per ottimizzare la dimensione delle immagini create, oltre che per mantenere i Dockerfiles leggibili e più facili da manutenere.

Vediamo un esempio pratico del suo utilizzo.

L'obiettivo è quello di creare una immagine che contenga un solo file ( README.md) , contenuto all'interno del file apache-tomcat-9.0.14.tar.gz
Il procedimento è quindi quello di:

* scompattare il tar
* copiare il file in una cartella desiderata ( /demo )
* cancellare i files estratti

Come immagine base si può utilizzare la busybox, la cui dimensione è di pochi mega.

## Build senza multi-stage

Dockerfile

```
FROM busybox
WORKDIR /demo/
ADD apache-tomcat-9.0.14.tar.gz .
RUN cp /demo/apache-tomcat-9.0.14/README.md /demo
RUN rm -fr /demo/apache-tomcat-9.0.14/

```

## Build multi-stage

Dockerfile

```
FROM busybox as builder
WORKDIR /demo/
ADD apache-tomcat-9.0.14.tar.gz .
RUN cp /demo/apache-tomcat-9.0.14/README.md /demo
RUN rm -fr /demo/apache-tomcat-9.0.14/

FROM busybox
WORKDIR /demo/
COPY --from=builder /demo/README.md .

```

Le immagini prodotte dalle due tipologie di build, di quanto differiscono in termini di dimensioni ?



