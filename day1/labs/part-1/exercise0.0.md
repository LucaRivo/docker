# Building Containers

## Introduzione

Durante la prima parte del workshop sono state utilizzate immagini presenti nel registry pubblico Docker Hub oppure fornite sotto forma di file .tar.
Uno degli aspetti più interessanti di Docker, è dato dal fatto che è possibile sviluppare delle proprie immagini e creare quindi dei Container custom.

Il processo di build prevede l'utilizzo di un Dockerfile. Si tratta di un file di testo, che contiene le istruzioni per la costruzione dell'immagine.
Questo insieme di istruzioni, viene fornito in input al demone Docker, che prima effettua una validazione formale, poi effettua la creazione dei vari layer dell'immagine, eseguendo la sequenza dei comandi.

Ogni istruzione è indipendente, nel senso che un ```RUN cd /tmp``` non ha effetto sull'istruzione successiva.

![Processo Build ](./images/docker-processo-build.png)

L'immagine verrà creata sul file system dell'host su cui viene effettuata la build. 
Occorrerà poi inserirla all'interno di un registry, opportunamente taggata, per poter essere distribuita ed utilizzata.

## Comando di build

Una volta composto il Dockerfile, il comando per la creazione dell'immagine è:

```bash

docker build – t <NOME_IMMAGINE> PATH_TO_DOCKERFILE

```

Il parametro ``` -t ``` permette di taggare l'immagine ed utilizzare quindi un nome.

Nel caso si utilizzi un file di nome diverso da Dockerfile, lo si può specificare con il parametro ``` -f <NOME_DOCKERFILE> ```

Quando si invoca il comando di build, tutto il contenuto della cartella e relative sottocartelle ( chiamato context ) viene passato al demone Docker.
Risulta quindi più semplice creare una cartella vuota per ogni immagine da creare, inserendoci solo il Doockerfile ed il files accessori necessari.

E' comunque possibile utilizzare un file .dockerignore dove specificare files, cartelle che non devono essere passati al demone Docker.




[Istruzioni](./exercise1.1.md).