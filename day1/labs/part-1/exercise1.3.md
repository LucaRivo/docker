# Istruzioni Dockerfile - Parte III

## Entrypoint

La sintassi è duplice:

```bash

ENTRYPOINT ["executable", "param1", "param2"] (exec form)
ENTRYPOINT command param1 param2 (shell form)

```

Questa istruzione permette di definire il comando che deve essere eseguito all'avvio del container.

Da questo punto di vista è molto simile a CMD, visto in precedenza. 

## Nota su Entrypoint e CMD

Le due istruzioni non sono mutuamente esclusive, ma presentano delle interazioni, in quanto CMD può essere usato per passare argomenti ad ENTRYPOINT.

In un Dockerfile si deve specificare almeno un CMD o un ENTRYPOINT

ENTRYPOINT specifica un comando che verrà sempre eseguito all'avvio del container.



## Volumes
```bash

VOLUME ["/PATH"] 

```

I volumes si possono configurare anche all'interno del Dockerfile. Con l'esecuzione della create del container il volume verrà creato ed effettuato il mount nel PATH specificato.

L'istruzione è equivalente a:

```bash

docker run -v $(docker volume create):[PATH] [IMAGE_NAME]

```

Esercizio: Configurare nel Dockerfile utilizzato per il container Tomcat, un volume che permetta di salvare i logs dell'application server. 

## User
```bash

USER <user>[:<group>]
USER <UID>[:<GID>]

```

Setta lo user name che verra' utilizzato per le istruzioni RUN, CMD, ENTRYPOINT che seguono nel Dockerfile.


# Best Practice

## Utilizzo di .dockerignore 

Quando si esegue una build, la cartella corrente viene definita build context.

Tutto il contenuto della build context e delle relative sottocartelle viene inviato al demone.
Questo rallenta la fase di building. Risulta quindi utile filtrare il contenuto con solo i files e cartelle utili per la build stessa.

Per questo si può utilizzare un file .dockerignore dove vengono specificate le regole da applicare, per identificare quali files non utilizzare per la build.

## Ridurre al minimo il tempo di build

- Mantenere la dimensione delle immagini, per quanto possibile ridotta
- Sfruttare i meccanismi build multistage
- Sfruttare l'istruzione FROM per utilizzare uno stack di immagini

## Evitare di installare tool non strettamente necessari

I tool "nice to have" contribuiscono a far crescere le immagini di dimensione. 
Poichè il numero di container in una realta' entrprise puo' raggiungere valori molti elevati, e' conveniente cercare di snellire al massimo le immagini.

## Definire uno standard a livello di label

Le label permettono di dare una classificazione ai container, che puo' essere sfruttata per semplificare le operazioni di gestione o di monitoraggio.
Per il nome delle label, si puo' utilizzare la reverse DNS notation, del tipo com.example.some-label per evitare confusioni di naming.


[Build multi-stage](./exercise1.4.md).






