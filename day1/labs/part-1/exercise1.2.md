# Istruzioni Dockerfile

## ENV

ENV è utilizzata per definire variabili d'ambiente. Lo scope di queste variabili è sia a compile che a runtime. Vengono quindi utilizzate sia durante il processo di build, che di esecuzione del container.

La sintassi è:


```bash

ENV <key>=<value> ... 

```

Ad esempio:

ENV CATALINA_HOME /usr/local/tomcat

```bash

ENV CATALINA_HOME="/opt/tomcat" 
ENV TOMCAT_MAJOR=8
ENV TOMCAT_VERSION=8.0.9

```

## RUN

La RUN permette di seguire comandi "all'interno dell'immagine". Il risultato è la creazione di un nuovo layer, che va ad aggiungersi allo stack di layer esistenti.

Esistono due sintassi per questa istruzione ( shell form ):

```bash

RUN <command>

```

oppure (exec form):

```bash

RUN ["executable", "param1", "param2"]

```

## CMD

La sintassi prevede tre forme:

```bash

    CMD ["executable","param1","param2"] (exec form, quella più utilizzata)
    CMD ["param1","param2"] (trattata in seguito, con l'istruzione ENTRYPOINT)
    CMD command param1 param2 (shell form)

```

Riportando un passaggio della documentazione riguardo la exec e shell form:

```bash

The exec form is parsed as a JSON array, which means that you must use double-quotes (“) around words not single-quotes (‘).

Unlike the shell form, the exec form does not invoke a command shell. 
This means that normal shell processing does not happen. 
For example, CMD [ "echo", "$HOME" ] will not do variable substitution on $HOME. 
If you want shell processing then either use the shell form or execute a shell directly, for example:
CMD [ "sh", "-c", "echo $HOME" ]. 

When using the exec form and executing a shell directly, as in the case for the shell form, 
it is the shell that is doing the environment variable expansion, not docker.

```

Serve per definire il comando di default, all'avvio del container.

Per questo motivo ci può essere un solo CMD per ogni immagine. Nel caso ce ne siano più di uno, viene considerato solo l'ultimo.

## Nota RUN/CMD

La RUN viene eseguita durante la creazione dell'immagine. Durante la fase di build Docker legge i comandi dal Dockerfile e costruisce i corrispondenti layer.

La CMD riguarda invece il runtime, per eseguire il main process del container. 

## EXPOSE 

```bash

EXPOSE <port>

```

Indica le porte che verranno esposte dal container.

Ad esempio:

```bash

EXPOSE 8080
EXPOSE 28015

```


## WORKDIR
```bash

WORKDIR /path/to/workdir

```

Setta la directory di lavoro in base al path settato ( creando la directory nel caso non esista). 
I comandi successivi ( tra quelli supportati, ovvero RUN, CMD, ENTRYPOINT, COPY, ADD ) faranno poi riferimento alla WORKDIR. 
Ad esempio:

```bash

WORKDIR /var
COPY pippo.lib ./lib 

```
L'effetto sarà di copiare il file pippo.lib all'interno di /var/lib

Si possono utilizzare più istruzioni WORKDIR. Se viene usato un path relativo, sarà dipendente dalla WORKDIR precedente.

Ad esempio:

```bash

WORKDIR /a
WORKDIR b
WORKDIR c
RUN pwd

```

L'output sarà ```/a/b/c```




## Hands On

Implementare due immagini di container.

La prima con un Tomcat 8 ( presente nella propria cartella folder ), partendo dall'immagine oracle/openjdk:8.

La seconda, che utilizza un FROM dalla prima, per pubblicare l'applicazione travel utilizzata in precedenza.





[Istruzioni, parte terza](./exercise1.3.md).