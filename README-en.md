# DOCKER-WORKSHOP

This project will help you to grasp the Docker technology.
The main outline for this course is to explain what are Docker
containers, how containers can be used to implement complex
systems, how Docker networking works and what are the common
actors of the Container Networking Model (CNM), what is
Docker Compose and how can it be used. Containers storage
will be also actively treated.

## Agenda

The workshop is organized in 4 distinct days. The agenda is as follows:

- 22nd of May, 2018: Day 0;

- 23rd of May, 2018: Day 1;

- 24th of May, 2018: Day 2;

- 25th of May, 2018: Day 3;

`N.B. All the material for this course can be found within the directories
day0, day1, day2 and day3.`

Specifically for this workshop, it was created a `Google Calendar`.
To access it, click this [calendar link](https://calendar.google.com/calendar/embed?src=frf93q7bsgft6n9gk6rceiglr8%40group.calendar.google.com&ctz=Europe%2FCopenhagen).

## Who this workshop is for

This project is targeted at system administrators, operations engineers,
DevOps engineers, and developers or stakeholders who are interested
in getting started with Docker from scratch.

`No prior experience with Docker containers is required.`

## Conventions used

There are a number of text conventions used throughout this book.

```CodeInText```: Indicates code words in text, database table names,
folder names, filenames, file extensions, path names, dummy URLs,
user input.

Here is an example: "The content of each layer is mapped to a special
folder on the host system, which is usually a sub-folder of 
```/var/lib/docker/```."

A block of code is set as follows:

```Dockerfile

COPY . /app
COPY ./web /app/web
COPY sample.txt /data/my-sample.txt
ADD sample.tar /app/bin/
ADD http://example.com/sample.txt /data/

```

When we wish to draw your attention to a particular part of a
code block, the relevant lines or items are surrounded by {{{{ }}}}:

```Dockerfile

FROM python:2.7
RUN mkdir -p /app
WORKDIR /app
COPY ./requirements.txt /app/
{{{{ RUN pip install -r requirements.tx }}}}
CMD ["python", "main.py"]

```

Any command-line input or output is written as follows:

```bash

mkdir ~/someDockerProjectDirectory

tree ~/someDockerProjectDirectory


```

## Contributions

__TODO__: Explain how to add new material to this project.

## Credits

__TODO__: Add here some acknowledges to people involved in this project.
