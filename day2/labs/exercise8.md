# VII. Binding delle porte
#### Esporta i servizi tramite binding delle porte

Solitamente una o più applicazioni web eseguono all'interno di un contenitore dettato della tecnologia utilizzata, ad esempio un HTTP server tipo *Apache* o un *servlet container* tipo *Tomcat*.
Una applicazione *twelve-factor* invece è completamente *self-contained* ed espone direttamente il servizio HTTP eseguendo il binding su una porta specifica. Sarà l'*host* di esecuzione che si occuperà di mappare tale porta tramite un *layer* di *routing*.

Per implementare questa modalità, spesso si procede con l'inclusione tramite [dipendenze](exercise3.md) di una libreria webserver ad esempio *Jetty*.

Nel nostro esempio tale implementazione è fornita direttamente dal framework *Spring Boot* il quale utilizza *Tomcat* come contenitore *embedded*. 
```xml
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
```

Il risultato della compilazione è quindi un *fat jar* che una volta eseguito espone direttamente il servizio HTTP sulla porta 8080.
L'ambiente di esecuzione *Docker* a questo punto potrà mappare tale porta a piacimento, esponendola ad altri servizi o verso l'esterno.

```
CONTAINER ID        IMAGE                                   COMMAND                  CREATED             STATUS              PORTS                                            NAMES
28b2e56a61d2        atsea_app                               "java -jar /app/AtSe…"   6 seconds ago       Up 3 seconds        0.0.0.0:5005->5005/tcp, 0.0.0.0:8080->8080/tcp   atsea-sample-shop-app_appserver_1

```

Notare come il mapping delle porte tramite il file di [config](exercise4.md) possa essere il meccanismo con cui una applicazione diventa a sua volta un [*baking service*](exercise4.md).

Procedere all'[esercizio 9](exercise9.md).