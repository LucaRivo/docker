# II. Dipendenze
#### Dipendenze dichiarate e isolate

Un'applicazione *twelve-factor* minimizza la sua dipendenza da librerie *system-wide* e dichiara invece in modo esplicito i suoi requisiti. Spesso si basa su tool di gestione delle dipendenze che tramite un file manifesto le esprime in modo *dichiarativo* ed eventualmente le installa tramite una procedura di *build*. Questo rende maggiormente predicibile il comportamento nell'ambiente di esecuzione, rende esplicite eventuali inter-dipendenze e non ultimo agevola l'introduzione degli sviluppatori all'ambiente di lavoro. 

Esempi di questo approccio nella nostra applicazione *demo* sono forniti da:
- il file `pom.xml` utilizzato da *Maven* nella componente di *backend*
- il file `package.json` utilizzato da *Yarn* o *NPM* nella componente di *frontend*

Lo stesso approccio è utilizzato nei tool di *containerizzazione* come Docker, il quale va a definire quali siano le dipendenze dell'**ambiente di esecuzione** delle nostre componenti applicative. Questo avviente principalmente tramite il `Dockerfile` e i comandi che vanno a comporre l'immagine del container. Notare come sia possibile adottare il concetto di *ereditarietà* partendo da un'immagine base con il comando `FROM`.

Nel nostro caso specifico inoltre vi è anche un esempio di distinzione nella gestione delle dipendenze per i flussi di **build** ed **esecuzione** dell'applicazione. Ad esempio le richieste di dipendenze per la compilazione dell'applicazione *React* sono molto diverse dalle dipendenze necessarie e sufficienti all'esecuzione del prodotto finale. A questo scopo il `Dockerfile` utilizza immagini base distinte (*multi-stage build*) oltre ad applicare operazioni diverse, ottimizzando entrambi i processi.

```
FROM node:latest AS storefront
WORKDIR /usr/src/atsea/app/react-app
...

FROM maven:latest AS appserver
WORKDIR /usr/src/atsea
...

FROM java:8-jdk-alpine
...
```
Procedere all'[esercizio 4](exercise4.md).