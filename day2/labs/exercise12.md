# XI. Log
#### Tratta i log come stream di eventi

I *log* sono essenziali per capire lo stato di salute di un'applicazione. Tradizionalmente il log di un'applicazione è gestito come *un* file memorizzato disco, fine.

La metodologia *twelve-factor* invece tende ad interpretare i *log* come un flusso di eventi generato dall'applicazione e dai suoi componenti. In questo modo le informazioni possono essere aggregate, processate, ri-analizzate a posteriori e quindi offrire numerosi scenari e funzionalità diverse.
Un'applicazione non dovrebbe preoccuparsi direttamente del proprio stream, ma semplicemente esporlo su **standard output**. Sarà l'ambiente di esecuzione che tratterà questo flusso nel modo più idoneo, ad esempio visulaizzandolo a video per uno sviluppatore o archiviandolo secondolo le politiche aziendali per un ambiente di produzione.

Nel mondo *container* questo approccio è condiviso ed accentuato dalla natura effimera degli stessi. Recuperare il log di un *container*, in particolare se si è presentata una situazione di errore, non è sempre immediato per cui l'approccio più indicato è quello di gestire tramite **driver** apposito il flusso dei log delle istanze in esecuzione.

Avviamo la nostra applicazione con una configurazione specifica che prevede l'utilizzo di **Fluentd** e l'accoppiata **ElasticSearch**+**Kibana**. L'obiettivo è quello di ottenere i log dei *container* su un sistema esterno sulla quale eseguire analisi e dashboarding.
Sono stati aggiunti i relativi servizi alla definizione, così da poterli avviare nello stesso demone:
```yaml
services:
    fluentd:
        build:
        context: fluentd
        ...
    elasticsearch:
        image: docker.elastic.co/elasticsearch/elasticsearch:6.2.4
        ...
    kibana:
        image: docker.elastic.co/kibana/kibana:6.2.4
        ...
```
Nella definizione dei container è stato quindi impostato un driver specifico che indirizza i log al servizio *fluentd*:
```yaml
    logging:
      driver: "fluentd"
      options:
        fluentd-address: localhost:24224
```
Avviamo con `docker-compose`:
```bash
docker-compose --file docker-compose-logging.yml up --build
````

Risulterà disponibile l'interfaccia *Kibana* all'indirizzo [http://<public_ip>:5601](http://<public_ip>:5601). L'accesso può avvenrire con le credenziali di default: *elastic* / *changeme*

### Nota:
Predisporre un *tunneling* della porta **5601** nel caso le impostazioni di rete non consentano l'accesso diretto. In questo caso l'indirizzo sarà <http://localhost:5601>


Procedere all'[esercizio 13](exercise13.md).