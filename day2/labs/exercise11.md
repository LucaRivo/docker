# X. Parità tra Sviluppo e Produzione
#### Mantieni lo sviluppo, staging e produzione simili il più possibile

Nello sviluppo software vi sono sempre state diferrenze (talvolta enormi) tra gli ambienti attraversati da un software. I sistemi di sviluppo sono lontanissimi dagli ambienti di esecuzione, così come gli ambienti di *staging* sono spesso per caratteristiche ed installato molto diversi da quelli di produzione. Questo porta spesso ad incompatibilità, differenze nei risultati attesi, problematiche nella gestione e molto altro.

Una *twelve-factor application* vuole azzerare il più possibile tali differenze, anche con gli ambienti di sviluppo, promuovendo il rilascio continuo (*Continous Deployment*).

L'adozione di *Docker* in tal senso porta notevoli vantaggi, basti pensare al motto: *"Build once, run anywhere"*. Nel nostro esempio il deploy dello *stack* è stato eseguito con immagini pre-compilate sul *registry* pubblico e non abbiamo fatto nessuna assunzione sull'ambiente di esecuzione. 

L'approccio *lightweight* dei container inoltre rende possibile molte operazioni anche negli ambienti di sviluppo o con poche risorse, ad esempio il setup di un *database* o di un *baking service*. 
Nel nostro esempio è stato eseguito un provisionine del database *PostgreSQL* per ciascuna situazione in modo del tutto autonomo, minimizzando quelle che potrebbe essere le incompatibilità nel passaggio tra uno *stage* e l'altro.

Procedere all'[esercizio 12](exercise12.md).