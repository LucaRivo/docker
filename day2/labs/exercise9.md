# VIII. Concorrenza
#### Scalare attraverso il process model

Un software in esecuzione è rappresentato da uno o più processi. Le modalità con cui sono gestiti questi processi hanno preso nel tempo svariate forme, ad esempio un insieme di sotto-processi figli di un unico padre (es. *Apache*) oppure un unico processo a cui sono assegnate molte risorse il quale gestisce internamente la concorrenza tramite *thread*. Tipicamente questi meccanismi non sono esposti direttamente nè devono essere gestiti esplicitamente dallo sviluppatore.

La metodologia *twelve-factor* invece tratta i processi come **first class citizens** prendendo spunto dal modello di esecuzione dei demoni di servizi *Unix*. Un'applicazione può essere composta quindi da diversi tipi di processo, ciascuno con il suo compito specifico. Associando questa gestione alle caratteristiche tipiche dei [processi](exercise7.md) *twelve-factor* ne deriva una gestione immediata e affidabile della scalabilità del servizio. 

![Process Model](./images/process-types.png)

Nel modello *Docker*, in particolare nei nodi *SWARM* questo approccio è evidente ed è il punto di forza nella gestione della scalabilità.

Riprendiamo il nostro esempio ed avviamo un ipotetico *stack* di produzione.
Predisponiamo i certificati e i *secrets* necessari:
```bash
mkdir certs

openssl req -newkey rsa:4096 -nodes -sha256 -keyout certs/domain.key -x509 -days 365 -out certs/domain.crt

docker secret create revprox_cert certs/domain.crt

docker secret create revprox_key certs/domain.key

docker secret create postgres_password certs/domain.key

echo production | docker secret create staging_token -
```

Quindi eseguiamo il *deploy* nel nostro *cluster*:
```bash
docker stack deploy -c docker-stack.yml atsea
```

Il servizio risulterà attivo, raggiungibile pubblicamente all'indirizzo [https://<public_ip>](https://<public_ip>) con i  processi replicati secondo specifica:
```bash
docker service ls

ID                  NAME                    MODE                REPLICAS            IMAGE                                                     PORTS
juclb2b71iw6        atsea_appserver         replicated          2/2                 dockersamples/atsea_app:latest                            
zgqp67qbg85j        atsea_database          replicated          1/1                 dockersamples/atsea_db:latest                             
8ccdpr6qziw5        atsea_payment_gateway   replicated          1/1                 dockersamples/atseasampleshopapp_payment_gateway:latest   
b0pcndkp4a5z        atsea_reverse_proxy     replicated          1/1                 dockersamples/atseasampleshopapp_reverse_proxy:latest     *:80->80/tcp,*:443->443/tcp
jbk7jvzejig5        atsea_visualizer        replicated          1/1                 dockersamples/visualizer:stable                           *:8001->8080/tcp
```

In caso di necessità, potremo scalare adeguatamente il servizio aumentando le repliche per il solo componente di cui abbiamo bisogno, ad esempio:
```bash
docker service scale atsea_appserver=4
```

### Extras
Visualizziamo *on-the-fly* lo stato del cluster tramite l'applicazione *SWARM Visualizer*. 
Dobbiamo mappare una porta nel nostro tunnel SSH per raggiungere l'applicazione tramite *localhost*. Utilizzando *Putty* possiamo usare la seguente configurazione, impostando però la porta **8001**.

![Putty Tunnel](./images/putty_tunnel.png)

Potremo quindi accedere all'applicazione dal nostro browser all'indirizzo: <http://localhost:8001>. Potremo anche filtrare la visualizzazione usando il nome dei container.

Fermiamo e ripuliamo l'ambiente rimuovendo l'intero *stack*:
```bash
docker stack rm atsea
```

Procedere all'[esercizio 10](exercise10.md).