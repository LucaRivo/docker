# VI. Processi
#### Esegui l’applicazione come uno o più processi stateless

Unìapplicazione è *running* nell'ambiente di esecuzione come uno o più processi anche di tipologia diversa. Secondo la metodologia *twelve-factor* questi processi dovrebbero essere **stateless** e non condividere tra loro alcuna risorsa. La persistenza di tutti i dati dovrebbe avvenire tramite *backing services* come ad esempio database o altri servizi di terze parti.

Il vantaggio di questo approccio apparirà evidente in altri fattori (es. [concorrenza](exercise9.md)) tuttavia non è sempre facile da implementare a livello software. Un esempio 'classico' è l'utilizzo delle *sticky sessions* nelle web application e la persistenza nella memoria del processo dei dati utente. Questo approccio viola la metodologia *twelve-factor* e spesso è un candidato per un *refactoring* architetturale mediante un *datastore* (es. *Redis* o *Memcached*).

Su questo aspetto l'utilizzo di *Docker* diventa in qualche modo un vincolo. I *container* sono per loro natura *stateless* ed effimeri e, al contrario dell'approccio tradizionale, diventa **difficile** gestire la persistenza ad esempio tramite la condivisione di volumi. Per questo motivo spesso il limite tecnologico diventa anche una ulteriore motivazione a reingegnerizzare il sistema nell'ottica di processi *stateless* e indipendenti.

Nel nostro esempio lanciamo l'applicazione in modalità di *test/staging* e verifichiamo il numero e la tipologia di processi running.
Per farlo creiamo dei certificati da utilizzare nel reverse proxy:
```bash
openssl req -newkey rsa:4096 -nodes -sha256 -keyout devsecrets/domain.key -x509 -days 365 -out devsecrets/domain.crt
```

Quindi avviamo la definizione base dell'applicazione con `docker-compose` in modalità *daemon* e seguiamo l'attività dei log:
```bash
docker-compose up --build -d
docker-compose logs --follow
```

L'applicazione è raggiungibile tramite reverse proxy all'inidirizzo: [https://<public_ip>](https://<public_ip>)

La lista dei processi dell'applicazione *running* può essere verificata riformattando l'output:
```
$ docker ps --format "table {{.ID}}\t{{.Image}}\t{{.Command}}\t{{.Status}}\t{{.Names}}"

CONTAINER ID        IMAGE                                   COMMAND                  STATUS              NAMES
28b2e56a61d2        atsea_app                               "java -jar /app/AtSe…"   Up 3 minutes        atsea-sample-shop-app_appserver_1
a1b1f16c65df        atsea-sample-shop-app_reverse_proxy     "nginx -g 'daemon of…"   Up 3 minutes        atsea-sample-shop-app_reverse_proxy_1
a929c41d840b        atsea_db                                "docker-entrypoint.s…"   Up 3 minutes        atsea-sample-shop-app_database_1
f119183849ae        atsea-sample-shop-app_payment_gateway   "/bin/sh -c /home/pa…"   Up 3 minutes        atsea-sample-shop-app_payment_gateway_1

``` 

Nell'esempio dell'applicazione *AtSea Shop* la gestione della sessione avviene tramite *token JWT* e quindi delegata al client.

![Token JWT](./images/token_jwt.png)

Eseguire lo stop e la 'pulizia' dell'ambiente:
```bash
docker-compose down --rmi 'all'
```

Procedere all'[esercizio 8](exercise8.md).