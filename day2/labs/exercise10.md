# IX. Rilasciabilità
#### Massimizzare la robustezza con avvii veloci e shutdown graduali

O meglio, con il termine originale *disposability*.
Un processo di una applicazione *twelve-factor* deve essere  rilasciabile ovvero avviato o fermato velocemente in qualsiasi momento. Questo comportamento massimizza la scalabilità del sistema e semplifica la sua gestione e configurazione.
Per essere rilasciabile un processo dovrebbe:
- avviarsi velocemente, migliorando lo *scaling* orizzontale
- terminare in modo graduale, progressivamente ignorando le nuove richieste e 'smaltendo' le richieste in corso prima di concludersi in modo definitivo
- nel caso di processi *worker* affidare la consistenza ad una gestione tramite coda, eventualmente riaccodando i lavori in corso

Queste caratteristiche sono tipicamente ottenibili tramite implementazione software, tuttavia *Docker* e *SWARM* rendono disponibili alcuni meccanismi di supporto ad esempio:
- *policy* di *restart*
- orchestrazione delle istanze dei servizi
- *reverse proxy* e *load balancing* dei servizi

Procedere all'[esercizio 11](exercise11.md).