# Applicazione di esempio

Nel nostro *tour* utilizzeremo un'applicazione come istanza di esempio su cui calare i 12 fattori e come si possono declinare nell'utilizzo di **Docker**. Chiaramente questo non vuole essere un esempio esaustivo ma l'intenzione è quella di far *percepire* quali siano gli impatti che questo tipo di approccio può dare direttamente allo sviluppo e alla gestione di una applicazione anche complessa, dalle caratteristiche tecnologiche eteregonea.

L'applicazione selezionata (**AtSea Shop**) è una *demo* di uno *storefront* composta di diversi elementi il cui schema può essere riassunto in:

![Architettura applicazione AtSea Shop](./images/atsea_component.png)

- *database PostgreSQL* per la persistenza delle informazioni
- *backend* realizzato in *JAVA* con *Spring Boot* che fornise le REST API principali di *business*
- *frontend* realizzato in *NodeJS* e *React* che fornisce la UI per gli utenti
- *gateway* di pagamento simulato da uno script *bash* per la gestione delle transazioni

Non sarà necessario conoscere le singole tecnologie per seguire le attività, è sufficiente identificarne il ruolo e le caratteristiche peculiari che presentano.

Procedere all'[esercizio 2](exercise2.md).