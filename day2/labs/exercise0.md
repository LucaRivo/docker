# La metodologia *Twelve-Factor App*
Gli scenari moderni vedono il software sempre più di frequentemente come un **servizio**. Si parla quindi di software as a service (SaaS). La *twelve-factor app* è una metodologia di sviluppo orientata alla costruzione di applicazioni che prediligono queste caratteristiche:

- utilizzano un **formato dichiarativo** per l’automazione della configurazione così da minimizzare i tempi e gli impatti dei cambiamenti;
- offrono la massima **portabilità** su vari ambienti di esecuzione, interfacciandosi in modo pulito con il sistema operativo sottostante;
- sono adatti allo sviluppo sulle recenti piattaforme **cloud**;
- minimizzano la **divergenza** tra sviluppo e produzione, supportando il *continuous deployment* e l'approccio *agile*;
- **scalano** significativamente senza troppi cambiamenti ai tool, all’architettura e al processo di sviluppo;

La metodologia *twelve-factor* è generalmente applicata al software, ma è pensata con un approccio *agnostico*. Non richiede cioè l'utilizzo di uno o l'altro linguaggio di programmazione, *framework* di sviluppo o tool specifico.
Nel nostro caso utilizzeremo questa metodologia come **riferimento** per quello che potremmo definire un **"software moderno"** e come questo approccio possa essere coniugato e supportato dagli strumenti di *containerizzazione* come **Docker**.

## Gli autori e lo scopo

Il manifesto della [Twelve-Factor App](https://12factor.net/) è disponibile pubblicamente ed è stato elaborato da persone che hanno collaborato direttamente e indirettamente allo sviluppo e al *deployment* di centinaia di applicazioni sulla piattaforma di sviluppo (PaaS) [Heroku](https://www.heroku.com/).
Le modalità con cui il software si evolve e gli strumenti con cui si affrontano le dificoltà nella gestione e nello *scaling* presentano determinati *pattern* che gli autori hanno voluto riassumere a beneficio comune, cercando di identificare un set di pratiche ideali che supportino la crescita dell'applicazione software nel tempo, ne agevolino la collaborazione del team di lavoro e riduca i costi legati all'obsolescenza e al mancato aggiornamento.

Il documento si rivolge testualmente a: "... ogni sviluppatore che costruisca applicazioni SaaS (Software As a Service), e a ogni ops che effettui il deploy e gestisca queste applicazioni...".

Procedere all'[esercizio 1](exercise1.md).