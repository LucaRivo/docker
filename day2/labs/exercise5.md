# IV. Backing Services
#### Tratta i *backing service* come “risorse”

La struttura delle applicazioni (in particolare se complesse) sta divenendo sempre più un *ecosistema* di servizi. Alla "classica" accoppiata *Database + Applicazione* si stanno aggiungendo sempre più servizi (locali o di terze parti) più o meno strutturali della soluzione. Talvolta l'applicazione stessa viene decomposta in servizi di entità minore e maggiormente governabili (**Microservizi**). 
La metodologia *twelve-factor* asseconda questo tipo di impostazione ed incentiva a trattare tutti i servizi allo stesso modo come **risorse connesse**.

![Codebase](./images/attached-resources.png)

I *backing service* devono essere quindi censiti ed esplicitati, configurando i parametri di connessione (tipicamente URL) e le eventuali chiavi di connessione. In questo modo risulta più facile tracciare le esigenze e seguire il deploy delle risorse che possono essere indipendenti o meno dall'applicazione.

La gestione di servizi esterni in Docker può avvenire tramite la configurazione delle variabili di ambiente come abbiamo già visto. Il valore aggiunto che si ottiene è invece nella gestione di servizi locali multipli chepuò avvenire tramite `docker-compose` o gli `stack SWARM`.

Nel nostro esempio `docker-compose.yml`:
```
services:
  reverse_proxy:
    networks: 
      - front-tier
...
  database:
    networks:
      - back-tier
...
  appserver:
    networks:
      - front-tier
      - back-tier
...
  payment_gateway:
    networks:
      - payment
```

I servizi vengono esplicitati chiaramente, configurando anche le eventuali comunicazioni su reti diverse sfruttando il *container networking model*.

Procedere all'[esercizio 6](exercise6.md).