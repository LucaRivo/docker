# I. Codebase
#### Una sola codebase sotto controllo di versione, tanti deployment

Parola d'ordine: **_VERSIONARE_**. Tutto. E' il modo con cui è possibile tracciare le modifiche, cotrollare l'evoluzione dell'applicazione e della sua configurazione, far collaborare più persone o anche più team di lavoro. Il controllo di versione aiuta inoltre a dirimere le situazioni di conflitto (tecnologico!) o seguire la storia di un componente o ricercare la _root cause_ di un malfunzionamento.

Una applicazione *twelve-factor* è quindi organizzata in una singola codebase dalla quale possono essere istanziati multipli *deploy*. 

![Codebase](./images/codebase-deploys.png)

La codebase è **singola** perchè stiamo parlando di **una** applicazione, diversamente potremmo parlare di un sistema distribuito (più applicazioni) o di una errata gestione delle dipendenze. Una codebase può essere organizzata in uno o più repository, in base alla tecnologia utilizzata (es. GIT con repository distribuiti), al modello organizzativo adottato e dalla complessità dell'applicazione stessa.

I **deploy** dell'applicazione possono essere **multipli** e differiscono per *versione* e *configurazione* pur provenendo dalla stassa *codebase*. Ad esempio la versione in esecuzione sull'ambiente di sviluppo dello sviluppatore potrebbe essere aggiornata agli ultimi *commit*, al contrario della versione di produzione stabile su tag specifici. Allo stesso modo gli ambienti di *staging* potrebbero presentare configurazioni limitate e temporanee diverse dagli ambienti di *produzione*.

Nel nostro esempio, procediamo al recupero della *codebase* della nostra applicazione, organizzata per semplicità su un'unico repository GIT.
```bash
git clone https://bitbucket.org/tzorzan_iks/atsea-sample-shop-app.git
```

Esploriamo il contenuto delle varie componenti dell'applicazione:
```bash
cd atsea-sample-shop-app
ls ./*
```

e verifichiamo come le componenti di Docker siano altrettanto versionate ed in stile *dichiarativo*:

```bash
cat app/Dockerfile
cat docker-compose.yml
cat docker-stack.yml
```

Procedere all'[esercizio 3](exercise3.md).