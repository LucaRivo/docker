# XII. Processi di Amministrazione
#### Esegui i task di amministrazione come processi una tantum

L'insieme dei processi di una applicazione in esecuzione non riguarda solo quelli *ordinari* ma talvolta sono necessarie delle operazioni *straordinarie*. Queste possono essere ad esempio delle migrazioni dati *una-tantum* o comandi di approfondimento per analizzare una situazione di errore. 

La metodologia *twelve-factor* indirizza questa esigenza preferendo i linguaggi che offrono una *shell REPL* (Read Eval Print Loop). In questo modo sarà possibile eseguire comandi e script in modo uniforme su tutti gli ambienti (dev/stage/prod), accedendo ad un ecosistema consistente dell'applicazione.

Negli ambienti *Docker* la modalità principale con cui eseguire i processi di amministrazione è il comando `docker exec`. Questo ci consente di eseguire comandi direttamente sui *container* in esecuzione, ereditando quindi il contesto.

Come esempio, avviamo la nostra applicazione ed apriamo una shell *sh* nel *container* dell'applicazione.

```bash
docker stack deploy -c docker-stack.yml atsea
docker service ps atsea_appserver --no-trunc --format "{{.Name}}.{{.ID}}"
docker exec -ti atsea_appserver.1.rdkziy4sqr8pgcv2npgfy0bal /bin/sh
```

In questo modo avremo un accesso praticamento completo all'istanza *running*.

Procedere all'[esercizio 14](exercise14.md).