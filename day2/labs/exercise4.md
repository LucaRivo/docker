# III. Configurazione
#### Memorizza le informazioni di configurazione nell’ambiente

La *configurazione* è l'elemento variabile tra un *deploy* e l'altro. Dovrebbe essere lunica differenza che determina un ambiente di test da uno di produzione o staging.
Questa separazione non è facile da ottenere nello sviluppo dell'applicazione, ma se ne sente particolarmente l'eseigenza quando ci si riferisce ad esempio a:
- parametri di connessione a database o altri servizi esterni
- parametri sensibili come password o chiavi di autenticazione
- valori specifici pe ril deployment (es. nomi delle sistance)

Nello sviluppo della applicazioni molti framework e librerie cercano di indirizzare la problematica della configurazione fornendo *ambienti* predefiniti (es. test/prod) e meccanismi di *override*. Tuttavia è chiaro come questo approccio in sistemi complessi possa portare ad una "esplosione" delle configurazioni possibili e, nel caso di pubblicazione della *codebase*, anche a problemi nell'esposizione di parametri sensibili.

Per questo motivo l'indirizzo delle *twelve-factor app* è quello di gestire la configurazione come **varibili di ambiente**. In questo modo possono essere gestiti in modo granulare e isolato tutti i parametri di configurazione necessari, sfruttando meccanismi anche eterogenei.

L'approccio di Docker e dei container segue pienamente questo indirizzo, facendo delle variabili d'ambiente il meccanismo "principe" per la configurazione di un'immagine. Inoltre sono disponibili costrutti specifici come i **secrets** per la gestione delle informazioni sensibili.

Vediamo come nella nostra applicazione di esempio i container siano configurati tramite le variabili di ambiente.

Il container database (derivato da un'immagine ufficiale PostgreSQL) può essere configurato nel nome utente, password e schema da istanziare.

```
database:
    image: dockersamples/atsea_db
    environment:
      POSTGRES_USER: gordonuser
      POSTGRES_DB_PASSWORD_FILE: /run/secrets/postgres_password
      POSTGRES_DB: atsea
...
    secrets:
      - postgres_password
```

Analogamente la componente app (JAVA Spring Boot) riutilizza i meccanismi messi a disposizione dal framework per accedere a runtime al segreto condiviso e disponibile presso l'ambiente di esecuzione Docker.

```
  appserver:
    image: dockersamples/atsea_app
...
    secrets:
      - postgres_password
```

E nel file `JpaConfiguration.java`:
``` Java
	/*
	 * Populate SpringBoot DataSourceProperties from application.yml 
	 */
	@Bean
	@Primary
	@ConfigurationProperties(prefix = "datasource.atsea")
	public DataSourceProperties dataSourceProperties() {
		DataSourceProperties dataSourceProperties = new DataSourceProperties();

		// Set password to connect to postgres using Docker secrets.
		try(BufferedReader br = new BufferedReader(new FileReader("/run/secrets/postgres_password"))) {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			dataSourceProperties.setDataPassword(sb.toString());
		} catch (IOException e) {
			System.err.println("Could not successfully load DB password file");
		}
		
		return dataSourceProperties;
	}
```
Procedere all'[esercizio 5](exercise5.md).