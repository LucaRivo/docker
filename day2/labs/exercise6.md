# V. Build, release, esecuzione
#### Separare in modo netto lo stadio di build dall’esecuzione

La *codebase* di una applicazione diventa una *deployment* attraversando tre fasi distinte:
- la fase di **build**, che converte il codice in una build “eseguibile”. E' determinata da una versione specifica del codice (*commit*), nella quale vengono compilati i binari con gli asset appropriati includeed incluse le eventuali dipendenze;
- la fase di **release** che a partire dal risultato della fase precedente lo combina con l’attuale insieme di *configurazioni* del deployment specifico. La release risultante contiene sia la build che le impostazioni;
- la fase di **runtime** vede l’applicazione in esecuzione nell’ambiente di destinazione, attraverso l’avvio di processi della release scelta.

![Processo di deploy](./images/release.png)

La *containerizzazione* di un'applicazione segue lo stesso approccio, distinguendo le fasi di build dei sorgenti, la creazione dell'immagine e l'esecuzione delle stessa.

**_Demo time_**

Ripercorriamo le stesse fasi utilizzando la variante di *sviluppo* della nostra applicazione tramite il file `docker-compose-dev.yml`.
```bash
docker-compose --file docker-compose-dev.yml build
```
Il sistema procederà alla build delle immagini dei *container* specificati, in particolare:
- procederà al download delle immagini base definite dai comandi `FROM` dei `Dockerfile`relativi
- eseguirà i passi di build degli artefatti configurati in ciascun `Dockerfile``
- creerà le immagini finali con il contenuto della compilazione

Possiamo vedere il risultato della compilazione eseguendo il *listing** delle immagini presenti localmente:
```bash
$ docker images

REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
atsea_app           latest              780b0ede6584        53 seconds ago       210MB
<none>              <none>              4af90a9fbd82        About a minute ago   775MB
<none>              <none>              14f00ec91a03        2 minutes ago        828MB
atsea_db            latest              4e19754be731        3 minutes ago        236MB
postgres            latest              61d053fc271c        3 days ago           236MB
node                latest              19cf4eb75395        3 days ago           675MB
maven               latest              afa912c3a613        2 weeks ago          635MB
java                8-jdk-alpine        3fd9dd82815c        15 months ago        145MB
```

Le immagine così generate potrebbero essere oggetto di *push* su *registry* remoti, utilizzando gli stessi tag o altri specifici che ne riconducono alla versione.
Ora possiamo eseguire la nostra applicazione nella sua versione appena compilata eseguendo il *run* della configurazione.
```bash
docker-compose --file docker-compose-dev.yml up
```

L'applicazione (container `appserver_1`) è avviata in modalità *debug* e attende la connesione per eseguire lo start. Aprire una seconda *shell*  ed eseguire un'istanza debugger `jdb` e collegandosi al socket remoto per avviare l'applicazione con i seguenti comandi:

```bash
jdb -attach 5005
run
exit
```

Visualizzare il risultato dell'applicazione nella fase di **running** accedendo all'indirizzo [http://<public_ip>:8080](http://<public_ip>:8080). I container attualmente in esecuzione possono essere visualizzati con un semplice `docker ps`. Terminare l'intera applicazione con `Ctrl+C`.

### Quiz
Il vantaggio dell'approccio *container* è quello di ridurre al minimo le dipendenze locali. Proviamo ad eseguire lo stesso comando `jdb` non dall'installazione JAVA locale, ma da un container predisposto allo scopo. Quale comando dovrei lanciare?

<!--
docker run -ti --net atsea-sample-shop-app_back-tier --name atsea-sample-shop-app_debugger java:8-jdk-alpine jdb -attach atsea-sample-shop-app_appserver_1:5005
-->

Procedere all'[esercizio 7](exercise7.md).