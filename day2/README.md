# Giorno 3

L'obiettivo della sessione è spostare progressivamente il focus da un contesto prettamente _infrastrutturale_ allo _sviluppo applicativo_. Si cercherà di dare una panoramica delle possibilità e dei vantaggi che offrono l'ecosistema Docker e la containerizzazione a nuovi paradigmi di sviluppo e gestione delle applicazioni. Come traccia di approfondimento saranno utilizzati i punti della metodologia *Twelve-Factor App*. Seguirà una sessione di _Question & Answers_ sulle tematiche affrontate in questa e nelle sessioni precedenti.

## Agenda

La sessione si articolerà nei seguenti temi:

1. Introduzione a alla *Twelve Factor App*
    - Cos'è e da chi è stata pensata
    - A chi si rivolge

2. I _Dodici Fattori_ in chiave Docker
    - I Codebase
    - II Dipendenze
    - III Configurazione
    - IV Backing Service
    - V Build, release, esecuzione
    - VI  Processi
    - VII Binding delle Porte
    - VIII Concorrenza
    - IX Rilasciabilità
    - X Parità tra Sviluppo e Produzione
    - XI Log
    - XII Processi di amministrazione

3. _Questions & Answers_

## Obiettivi

L'obiettivo della sessione è quello di prospettare le possibile evoluzioni offerte dalla containerizzazione e da Docker nello sviluppo e nella gestione delle Applicazioni Software.

Procedere all'[esercizio 0](./labs/exercise0.md).