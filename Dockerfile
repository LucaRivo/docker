ARG TOMCAT_VERSION=9
FROM tomcat:${TOMCAT_VERSION}
LABEL com.luca.project="docker"
LABEL com.luca.description="prima immagine docker"
LABEL license="not free"
ADD travel.war /usr/local/tomcat/webapps/
