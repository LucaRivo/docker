# DOCKER-WORKSHOP

Questo workshop ha l'obiettivo di introdurre, anche con esempi pratici la tecnologia Docker.
Gli argomenti partono dai concetti di base, per arrivare a comprendere 
come i container possono essere utilizzati per implementare
complessi sistemi distribuiti.

## Agenda

Il workshop è strutturato in 3 giorni.
L'agenda è come segue:

- Day 0: concetti introduttivi e persistenza tramite volumi

- Day 1: build di immagini, utilizzo dell'orchestratore Swarm

- Day 2: best practice, reference architecture 


`N.B. Tutto il materiale del corso può essere trovate nelle seguenti directory day0, day1, day2.`

## A chi è dedicato

Il workshop è dedicato a system administrator, operations engineer,
DevOps engineer, developer e architects.

`Non sono necessarie conoscenze preliminari su Docker, mentre occorre conoscere i principali comandi del sistema oprativo Linux.`

## Convenzioni utilizzate

Abbiamo definito un insieme di convenzioni utilizzate nel materiale qui raccolto.

`CodeInText`: Indica parole nel testo del codice, nomi di directory,
nomi di file, estensioni di file, nomi di path, URL, input utente.

Questo è un esempio: "The content of each layer is mapped to a special
folder on the host system, which is usually a sub-folder of 
```/var/lib/docker/```."

Un blocco di codice è come segue:

```Dockerfile

COPY . /app
COPY ./web /app/web
COPY sample.txt /data/my-sample.txt
ADD sample.tar /app/bin/
ADD http://example.com/sample.txt /data/

```

In caso in cui vorremmo focalizzare la tua attenzione in un blocco di codice
utilizzeremo questo simbolo {{{{ }}}}:

```Dockerfile

FROM python:2.7
RUN mkdir -p /app
WORKDIR /app
COPY ./requirements.txt /app/
{{{{ RUN pip install -r requirements.tx }}}}
CMD ["python", "main.py"]

```

Ogni input a riga di comando oppure output è indicato come segue:

```bash

mkdir ~/someDockerProjectDirectory

tree ~/someDockerProjectDirectory

```


